﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DualResonance.Deployables.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DualResonance.Deployables.Cards;

namespace DualResonance.Deployables.Units.Tests
{
    [TestClass()]
    public class ActiveMercenaryTests
    {
        [TestMethod()]
        public void ActiveMercenaryTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ModifyStatsTest()
        {
            var merc = new ActiveMercenary(MercenaryCards.GetMercenary("Ekethus"), Teams.Alpha);
            merc.ModifyStats(-3, -3, -1);
            Assert.AreEqual(7, merc.AttackPower);
            Assert.AreEqual(7, merc.WillPower);

            merc.ModifyStats(2, 2, 1);
            Assert.AreEqual(9, merc.AttackPower);
            Assert.AreEqual(9, merc.WillPower);

            merc.Tick();

            Assert.AreEqual(7, merc.AttackPower);
            Assert.AreEqual(7, merc.WillPower);
        }

        [TestMethod()]
        public void TickTest()
        {
            var merc = new ActiveMercenary(MercenaryCards.GetMercenary("Ekethus"), Teams.Alpha);
            merc.Ailment = new Status.PoisonStatus(-1, 1, Status.StatusPrecedence.High);
            merc.Tick();
            Assert.AreEqual(9, merc.HealthPoints);
        }
    }
}
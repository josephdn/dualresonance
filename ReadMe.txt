+-----------------------------------------------------------+
| Dual Resonance: A Deck-Building Tactics Game (Eventually) |
+-----------------------------------------------------------+

RULES
-----
In the demo, you play as Copper Bitner, a commander. As a commander, you have
the ability to generate both gold and resonance. Gold is used to place allies
on the battlefield, while resonance is used to pay for the abilities of your
allies. You can only have one of each unit on the field, and a maximum of 15
gold at any time. Units can move around, use abilities, and attack. After
moving, the unit can attack or use an ability, but if an attack or an ability
is used, they have to wait until the next turn before they can do anything.

In order to attack the enemy commander, move your units to the opposite side of
the battlefield, where, upon selecting the unit, an option labled "Att. Cmmdr"
will appear.

KEYWORDS
--------
In some abilities, there are various keywords that will be useful to know. They
are all related to passive abilities.

VENGEANCE: Such abilities activate when the unit dies. An example of this to be
aware of is [Silvermaiden], whose <Metalwrought> ability makes her come back to
life, albeit a bit weaker.

CONCENTRATE: These abilities activate when the unit's stats are modified. At
the moment, no units have a Concentrate ability; one of them is <Extra Mile>,
which doubles the stat changes, both positive and negative, that the unit gets.

RETALIATE: Abilities with Retaliate activate after the user's been attacked,
and before death (so they always trigger).

FEEDBACK: Triggers when the unit is targeted by an ability. An example is
<Shared Knowledge>, which grants an ally invoker (the one casting the ability)
+1 WP.

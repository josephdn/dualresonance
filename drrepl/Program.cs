﻿using System;
using DualResonance;
using DualResonance.Deployables;
using DualResonance.Deployables.Cards;
using DualResonance.Deployables.Units;

namespace drrepl
{
    class Program
    {
        public static GameSession session;
        public static ActiveMercenary Queried;

        public static Player Player = new Player() { PlayerId = 1, Team = Teams.Alpha };

        public static AIPlayer ai = new AIPlayer() { PlayerId = 2, Team = Teams.Beta };

        private static void WriteQueried()
        {
            Console.SetCursorPosition(0, 0);
            Console.Write("QUERY RESULT");
            Console.SetCursorPosition(0, 1);
            if (Queried == null)
                Queried = new ActiveMercenary(MercenaryCards.Nothing, Teams.Neutral);
            Console.Write("{0}", Queried.Name);
            Console.SetCursorPosition(0, 2);
            Console.Write("[HP: {0}/{1}]", Queried.HealthPoints, Queried.MaxHP);
            Console.SetCursorPosition(0, 3);
            Console.Write("[AP: {0}/{1}]", Queried.AttackPower, Queried.MaxAP);
            Console.SetCursorPosition(0, 4);
            Console.Write("[WP: {0}/{1}]", Queried.WillPower, Queried.MaxWP);
            Console.SetCursorPosition(0, 5);
            string abilityName;
            int barReq, psyReq, disReq, burReq, triReq;
            if (Queried.A == null)
            {
                abilityName = "";
                barReq = psyReq = disReq = burReq = triReq = 0;
            }
            else
            {
                abilityName = Queried.A.Name;
                barReq = Queried.A.BarrierRequirement;
                burReq = Queried.A.BurstRequirement;
                psyReq = Queried.A.PsycheRequirement;
                disReq = Queried.A.DistortRequirement;
                triReq = Queried.A.TriageRequirement;
            }
            Console.Write("[A: {0}] {1}/{2}/{3}/{4}/{5}", abilityName, barReq, burReq, psyReq, disReq, triReq);
            Console.SetCursorPosition(0, 6);
            if (Queried.B == null)
            {
                abilityName = "";
                barReq = psyReq = disReq = burReq = triReq = 0;
            }
            else
            {
                abilityName = Queried.B.Name;
                barReq = Queried.B.BarrierRequirement;
                burReq = Queried.B.BurstRequirement;
                psyReq = Queried.B.PsycheRequirement;
                disReq = Queried.B.DistortRequirement;
                triReq = Queried.B.TriageRequirement;
            }
            Console.Write("[B: {0}] {1}/{2}/{3}/{4}/{5}", abilityName, barReq, burReq, psyReq, disReq, triReq);
            Console.SetCursorPosition(0, 7);
            string str = Queried.Boon == null ? "None" : Queried.Boon.ToString();
            Console.WriteLine("[{0}/{1}]", Queried.disabled == null ? "Active" : Queried.disabled.ToString(), Queried.CanAct);
            Console.WriteLine("[Boon: {0}]", str);
            Console.WriteLine("[Ailment: {0}]", Queried.Ailment == null ? "None" : Queried.Ailment.ToString());
        }

        static void WriteMercs(DeckInfo deck, int leftmost = 80)
        {
            Console.SetCursorPosition(leftmost - 5, 0);
            Console.Write("MERCS");
            MercenaryCard current;
            for (int i = 0; i < 8; i++)
            {
              
                current = deck.GetMercenary(i);
                string name = current != null ? current.Name : "---";
                
                Console.SetCursorPosition(leftmost - name.Length - 4, i + 1);
                Console.Write("{0} - {1}", name, i);
            }
        }

        static void WriteBoard(int xPos = 40, int yPos = 0)
        {
            Console.SetCursorPosition(xPos, yPos);
            string[] rowData = new string[5];
            for (int y = 0; y < 5; y++)
            {
                char[] dat = new char[5];
                for (int x = 0; x < 5; x++)
                {
                    IActive a = session.Query(new BoardCoordinate(x, y));
                    dat[x] = a == null ? '.' : ((ActiveMercenary)a).Name[0];
                }
                rowData[y] = string.Format(
                    "{0} {1} {2} {3} {4} {5}", (char)('A' + y), 
                    dat[0],
                    dat[1],
                    dat[2],
                    dat[3],
                    dat[4]);
            }
            Console.Write("  1 2 3 4 5");
            Console.SetCursorPosition(xPos, ++yPos);
            Console.Write(rowData[0]);
            Console.SetCursorPosition(xPos, ++yPos);
            Console.Write(rowData[1]);
            Console.SetCursorPosition(xPos, ++yPos);
            Console.Write(rowData[2]);
            Console.SetCursorPosition(xPos, ++yPos);
            Console.Write(rowData[3]);
            Console.SetCursorPosition(xPos, ++yPos);
            Console.Write(rowData[4]);
            Console.SetCursorPosition(xPos, ++yPos);
            Console.Write("Current Player: {0}", session.ActivePlayer.PlayerId);
            Console.SetCursorPosition(0, 11);
            var commander = session.GetPlayerCommander(Player);
            Console.WriteLine("Commander: {0}, {1}", commander.Name, commander.Epithet);
            Console.WriteLine(
                "HP/{0} Gold/{1} Resonance/{2}", 
                commander.HealthPoints, 
                commander.CurrentGold, 
                commander.CurrentResonance
            );
            Console.WriteLine("{0}|{1}", commander.PrimaryResonance, commander.SecondaryResonance);

            Console.WriteLine("Current Player: {0}", session.ActivePlayer.PlayerId);
        }

        static void Main(string[] args)
        {
            session = new GameSession();
            
            Console.Title = "Dual Resonance REPL/Test Environment";
            DeckInfo deck = new DeckInfo();
            //for (int i = 0; i < 8; i++)
            deck.AddMercenary(1);
            deck.AddMercenary(6);
            deck.AddMercenary(3);
            deck.AddMercenary(2);
            deck.AddMercenary(4);
            deck.AddMercenary(5);
            //Queue<string> logMessages = new Queue<string>(9);
            deck.Commander = Commanders.Copper;
            Player.CurrentDeck = deck;
            ai.CurrentDeck = deck;

            session.AddPlayer(Player);
            session.AddPlayer(ai);
            session.Initialize();

            session.Spawn(ai, new BoardCoordinate(0, 0), 0);
            session.Spawn(ai, new BoardCoordinate(4, 0), 3);
            string message = "";
            int successfulAIMoves = 0;
            int failedAIMoves = 0;
            while (true)
            {
                Console.Clear();
                WriteQueried();
                Console.WriteLine("\t---");
                WriteMercs(deck);
                WriteBoard();
                Console.SetCursorPosition(0, 15);
                Console.WriteLine(message);

                string[] nargs = Console.ReadLine().Split(' ');
                string command = nargs[0];
                try
                {
                    Commands.Execute(command, nargs);
                    message = "";
                }
                catch (Exception e) when (e is ArgumentException || e is InvalidOperationException)
                {
                    message = e.Message;
                    //logMessages.Enqueue(e.Message);
                    Console.WriteLine(e.Message);
                }
                if (session.ActivePlayer != Player)
                {
                    foreach (var c in ai.BuildTurn(session))
                    {
                        bool success = c.Apply(session, out string msg);
                        if (success) successfulAIMoves++;
                        else failedAIMoves++;
                    }
                }
            }
        }
    }
}

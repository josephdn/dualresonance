﻿using System;
using System.Collections.Generic;
using DualResonance;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DualResonance.Deployables.Units;

namespace drrepl
{
    static class Commands
    {
        private static Dictionary<string, Action<string[]>> commands = new Dictionary<string, Action<string[]>>
        {
            #region Game calls
            // Queries the unit at the location.
            { "q", (args) => Query(args) },
            { "query", (args) => Query(args) },
            // Moves the unit at the location 1-2 spaces in the specified direction.
            { "mv", (args) => Move(args) },
            { "move", (args) => Move(args) },
            // Causes the unit in the specific location to attack the unit in the specified direction.
            { "at", (args) => Attack(args) },
            { "attack", (args) => Attack(args) },
            // Causes the unit at the location to use the chosen ability in the specified direction.
            { "ab", (args) => Ability(args) },
            { "ability", (args) => Ability(args) },
            // Causes a unit to spawn in the chosen location.
            { "sp", (args) => Spawn(args) },
            { "spawn", (args) => Spawn(args) },
            // Causes the unit at the specified location to rest (becomes disabled).
            { "r", (args) => Rest(args) },
            { "rest", (args) => Rest(args) },
            { "t", (args) => Program.session.Tick() },
            #endregion

            #region REPL-specific
            // Exits the application.
            { "exit" , (args) => Environment.Exit(0) },
            { "ex", (args) => Environment.Exit(0) },
            // Displays the available commands
            { "?", (args) => Console.WriteLine("help isn't implemented yet") },
            { "help", (args) => Console.WriteLine("help isn't implemented yet") },
            // Restarts the game by resetting the correlated variables
            { "rs", (args) => Console.WriteLine("Restart isn't implemented yet") },
            { "restart", (args) => Console.WriteLine("Restart isn't implemented yet") },
            #endregion
        };

        public static void Execute(string command, string[] args)
        {
            try
            {
                commands[command.ToLower()](args);
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Could not find command `{0}`.", command);
            }
        }

        private static void Query(string[] args)
        {
            if (args.Length != 2)
                throw new ArgumentException("Correct usage: {0} [location]", args[0]);
            BoardCoordinate look_at = new BoardCoordinate(int.Parse(args[1][0].ToString()) - 1, int.Parse(args[1][1].ToString()) - 1);
            Program.Queried = (ActiveMercenary)Program.session.Query(look_at);
        }

        private static void Move(string[] args)
        {
            BoardCoordinate look_at = new BoardCoordinate(int.Parse(args[1][0].ToString()) - 1, int.Parse(args[1][1].ToString()) - 1);
            Direction direction;
            switch (args[2].ToLower())
            {
                case "n":
                    direction = Direction.North;
                    break;
                case "s":
                    direction = Direction.South;
                    break;
                case "e":
                    direction = Direction.East;
                    break;
                case "w":
                    direction = Direction.West;
                    break;
                default:
                    throw new ArgumentException("Invalid direction given.");
            }
            Program.session.MoveUnit(Program.Player, look_at, BoardCoordinate.FromDirection(direction));
        }

        public static void Attack(string[] args)
        {
            BoardCoordinate pos = new BoardCoordinate(int.Parse(args[1][0].ToString()) - 1, int.Parse(args[1][1].ToString()) - 1);
            Direction dir;
            switch (args[2].ToLower())
            {
                case "n":
                    dir = Direction.North;
                    break;
                case "s":
                    dir = Direction.South;
                    break;
                case "e":
                    dir = Direction.East;
                    break;
                case "w":
                    dir = Direction.West;
                    break;
                default:
                    throw new ArgumentException("Invalid direction given.");
            }
            Program.session.InvokeAttack(Program.Player, pos, dir);
        }

        public static void Ability(string[] args)
        {
            //pos abptr dir
            if (args.Length != 4)
                throw new ArgumentException("Syntax: (xy) [a|b] (dir)");
            BoardCoordinate pos = new BoardCoordinate(int.Parse(args[1][0].ToString()) - 1, int.Parse(args[1][1].ToString()) - 1);
            AbilityId abptr = args[2][0] == 'a' ? AbilityId.A : AbilityId.B;
            Direction dir;
            switch (args[3].ToLower())
            {
                case "n":
                    dir = Direction.North;
                    break;
                case "s":
                    dir = Direction.South;
                    break;
                case "e":
                    dir = Direction.East;
                    break;
                case "w":
                    dir = Direction.West;
                    break;
                default:
                    throw new ArgumentException("Invalid direction given.");
            }
            Program.session.InvokeAbility(Program.Player, pos, dir, abptr);
        }

        private static void Spawn(string[] args)
        {
            if (args.Length != 3)
                throw new ArgumentException("The provided arguments didn't add up. Usage: spawn [mercenary-number] [coordinate]");
            try
            {
                Program.session.Spawn(
                    Program.Player,
                    new BoardCoordinate(int.Parse(args[2][0].ToString()) - 1, int.Parse(args[2][1].ToString()) - 1),
                    int.Parse(args[1])
                    );
            }
            catch (IndexOutOfRangeException)
            {
                throw new ArgumentException("One of the args was malformed. Check your syntax and try again. Usage: spawn [mercenary-number] [coordinate]");
            }
        }

        private static void Rest(string[] args)
        {
            if (args.Length != 2)
                throw new ArgumentException("Usage: rest|r [coord]");
            Program.session.Rest(
                Program.Player,
                new BoardCoordinate(
                    int.Parse(args[1][0].ToString()) - 1,
                    int.Parse(args[1][1].ToString()) - 1
                )
            );
        }
    }
}

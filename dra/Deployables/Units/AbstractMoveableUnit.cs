﻿using DualResonance.Deployables.Units.States;

namespace DualResonance.Deployables.Units
{
    public abstract class AbstractMoveableUnit : ActiveBase, ILocatable
    {
        /// <summary>
        /// The location of this unit on the board.
        /// </summary>
        public BoardCoordinate Location
        {
            get;
            set;
        }

        /// <summary>
        /// Determines if this unit can move.
        /// </summary>
        public bool CanMove
        {
            get => UnitState is SpryUnitState;
        }

        protected AbstractMoveableUnit(Teams team) : base(team) { }
        /// <summary>
        /// Determines the direction to another unit.
        /// </summary>
        /// <param name="other">The unit to check against.</param>
        /// <returns>The direction that the other unit resides in.</returns>
        public Direction DirectionTo(ILocatable other)
        {
            return Location.DirectionTo(other.Location);
        }

        /// <summary>
        /// Moves the unit one space in the specified direction.
        /// </summary>
        /// <param name="dir">The direction in which the unit should move.</param>
        public void Move(Direction dir)
        {
            UnitState.Move(this, dir);
        }

        /// <summary>
        /// Changes the unit's location.
        /// </summary>
        /// <param name="newLocation">The new location of the unit.</param>
        public void Move(BoardCoordinate newLocation)
        {
            UnitState.Move(this, newLocation);
        }

        /// <summary>
        /// Determines if the unit is next to another.
        /// </summary>
        /// <param name="other">The unit to check against.</param>
        /// <returns>True if the other unit is adjacent to this one.</returns>
        public bool NextTo(ILocatable other)
        {
            return Location.DistanceTo(other.Location) == 1;
        }
    }
}

﻿namespace DualResonance.Deployables
{
    interface IMercenary : ICharacter
    {
        int MaxAP { get; }
        int MaxWP { get; }
    }
}

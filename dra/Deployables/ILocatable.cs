﻿namespace DualResonance.Deployables
{
    /// <summary>
    /// Identifies an object that can be moved around the game board.
    /// </summary>
    public interface ILocatable
    {
        /// <summary>
        /// Retrieves the locations of this object.
        /// </summary>
        BoardCoordinate Location { get; }
        
        /// <summary>
        /// If the unit can move at all.
        /// </summary>
        bool CanMove { get; }

        /// <summary>
        /// Determines if this object is adjacent (4-neighbor) to another.
        /// </summary>
        /// <param name="other">The ILocatable to check against.</param>
        /// <returns>True if this is next to another.</returns>
        bool NextTo(ILocatable other);

        /// <summary>
        /// Determines the direction the other ILocatable resides in.
        /// </summary>
        /// <param name="other">The unit to check against.</param>
        /// <returns>The direction.</returns>
        Direction DirectionTo(ILocatable other);

        /// <summary>
        /// Moves the object to the new location.
        /// </summary>
        /// <param name="newLocation">The place where the unit should be placed.</param>
        void Move(BoardCoordinate newLocation);

        /// <summary>
        /// Moves the object in the specified direction.
        /// </summary>
        /// <param name="dir">The direction in which the unit should move.</param>
        void Move(Direction dir);
    }
}

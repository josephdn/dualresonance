﻿namespace DualResonance.Deployables.Cards
{
    // TODO: Enable attributes of a mercenary.
    // TODO: Add cost for mercenary, including in constructor.
    public class MercenaryCard : CharacterCard, IMercenary
    {       
        
        /// <summary>
        /// The maximum WP available to the mercenary.
        /// </summary>
        public int MaxWP { get; private set; }

        /// <summary>
        /// The maximum HP available to the mercenary.
        /// </summary>
        public int MaxHP { get; private set; }

        /// <summary>
        /// The maximum AP available to the mercenary.
        /// </summary>
        public int MaxAP { get; private set; }

        private readonly int goldCost;
        public override int GoldCost => goldCost;

        public readonly int Id;

        public MercenaryCard(int maxHP, int maxAP, int maxWP, int goldCost, int id)
        {
            MaxHP = maxHP;
            MaxAP = maxAP;
            MaxWP = maxWP;
            this.goldCost = goldCost;
            Id = id;
        }
    }
}

﻿using DualResonance.Abilities;

namespace DualResonance.Deployables.Cards
{
    public class CommanderCard : ICharacterCard, ICommander
    {
        public const int COMMANDER_MAX_HP = 30;

        public Ability A { get; set; }

        public Ability B { get; set; }
        public int GoldPerTurn { get; set; }
        public int ResonancePerTurn { get; set; }
        public ResonanceType PrimaryResonance { get; set; }
        public ResonanceType SecondaryResonance { get; set; }

        public ResonanceType GeneratedResonances
        {
            get
            {
                return PrimaryResonance | SecondaryResonance;
            }
        }

        public int MaxHP => COMMANDER_MAX_HP;

        public string Name { get; set; }

        public string Epithet { get; set; }

        public string FlavorText { get; set; }
    }
}

﻿using DualResonance.Abilities;
using System.IO;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace DualResonance.Deployables.Cards
{
    public static class UnitImporter
    {

        public static void LoadMercenaries(string path)
        {
            string json = File.ReadAllText(path);

            JObject o = JObject.Parse(json);
            /*****************************************************************\
             * These will eventually need to be used, especially once I want *
             * to fully support "modding" or at least custom characters.     *
            \*****************************************************************/ 
            //string set = (string)o["set"];
            //string code = (string)o["code"];
            //string description = (string)o["description"];
            //string ability_lookup = (string)o["ability_lookup"];

            JArray units = (JArray)o["units"];
            foreach (JToken token in units)
            {
                int uid = (int)token["id"];
                int hp = (int)token["hp"];
                int ap = (int)token["ap"];
                int wp = (int)token["wp"];
                int cost = (int)token["cost"];
                // TODO: implement multiple language integration.
                JToken langData = token["eng"];
                string name = (string)langData["name"];
                string flavor = (string)langData["flavor"];

                MercenaryCard unit = new MercenaryCard(hp, ap, wp, cost, uid)
                {
                    Name = name,
                    FlavorText = flavor,
                    A = RetrieveAbility(token, AbilityId.A),
                    B = RetrieveAbility(token, AbilityId.B)
                };
                
                MercenaryCards.AddMercenary(unit);
            }
        }
        
        private static Ability RetrieveAbility(JToken token, AbilityId ability)
        {
            string key = ability == AbilityId.A ? "a" : "b";
            var value = token[key];
            if (value.Type == JTokenType.Null) return null;
            return (Ability)typeof(AbilityList).GetField((string)value, BindingFlags.Static | BindingFlags.Public).GetValue(null);
        }
    }
}

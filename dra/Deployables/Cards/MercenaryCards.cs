﻿using DualResonance.Abilities;
using System.Collections.Generic;

namespace DualResonance.Deployables.Cards
{
    public static class MercenaryCards
    {
        public const int NothingId = 0;
        public static readonly MercenaryCard Nothing = new MercenaryCard(1, 0, 0, 0, 0)
        {
            Name = "None",
            FlavorText = @"",
            A = null,
            B = null,
        };
        #region Static Merc Definitions
        /*public const int SelflessCompatriotId = 1;
        public static readonly MercenaryCard SelflessCompatriot = new MercenaryCard(4, 2, 8, 5, SelflessCompatriotId)
        {
            Name = "Selfless Compatriot",
            FlavorText = "Truly, one of the most loyal of companions.",
            A = AbilityList.TEST_ForceTick,
            B = AbilityList.TEST_RemoveImmediately
        };

        public const int ProfessorId = 2;
        public static readonly MercenaryCard Professor = new MercenaryCard(1, 1, 5, 2, ProfessorId)
        {
            Name = "Prof. Trebeg",
            FlavorText = "Never did care for giving extra credit assignments.",
            A = AbilityList.MindBlow,
            B = AbilityList.None,
        };

        public const int VerdanRedpetalId = 3;
        public static readonly MercenaryCard VerdanRedpetal = new MercenaryCard(2, 1, 4, 1, VerdanRedpetalId)
        {
            Name = "Redpetal",
            FlavorText = "Some factions of Verdan society raise more than one kind of beautiful flower.",
            A = AbilityList.Beguile,
            B = AbilityList.None,
        };

        public const int PyranGunnerId = 4;
        public static readonly MercenaryCard PyranGunner = new MercenaryCard(3, 3, 1, 2, PyranGunnerId)
        {
            Name = "Foros",
            FlavorText = "\"WOOHOO!\" - Aldre Fosfor, character in the film 'Ablaze'.",
            A = AbilityList.SmallPayload,
            B = AbilityList.None,
        };

        public const int CloudPilotId = 5;
        public static readonly MercenaryCard CloudPilot = new MercenaryCard(1, 3, 3, 1, CloudPilotId)
        {
            Name = "Ribbon",
            FlavorText = "Though the skies were always welcoming, nothing captured her like the mystery of soaring through clouds.",
            A = AbilityList.SmallPayload,
            B = AbilityList.None,
        };

        // Lyrio Vaye
        public const int EnticingAssassinId = 6;
        public static readonly MercenaryCard EnticingAssassin = new MercenaryCard(6, 6, 6, 6, EnticingAssassinId)
        {
            Name = "Lyrio",
            FlavorText = "Many hearts were smitten by her, first by her beauty, then by her knife.",
            A = AbilityList.Beguile,
            B = AbilityList.None,
        };

        // Amarine
        public const int LoyalSquireId = 7;
        public static readonly MercenaryCard LoyalSquire = new MercenaryCard(2, 2, 2, 2, LoyalSquireId)
        {
            Name = "Amarine",
            FlavorText = "All resonance knights get squires, but few are as loyal as her.",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        // Beatrice "Triss" Coombs
        public const int BeeGirlId = 8;
        public static readonly MercenaryCard BeeGirl = new MercenaryCard(4, 1, 4, 3, BeeGirlId)
        {
            Name = "Bee Girl",
            FlavorText = "Triss's personality would do one of two things to you: make optimists happier or make pessimists saltier.",
            A = AbilityList.Encourage,
            B = AbilityList.Discourage,
        };

        public const int BittersweetSageId = 9;
        public static readonly MercenaryCard BittersweetSage = new MercenaryCard(2, 1, 5, 3, BittersweetSageId)
        {
            Name = "Samlock",
            FlavorText = "Her blessings on those who sought her became curses on others.",
            A = AbilityList.HemlockBlessing,
            B = AbilityList.RefreshingAroma
        };

        public const int SilvermaidenId = 10;
        public static readonly MercenaryCard Silvermaiden = new MercenaryCard(2, 3, 4, 8, SilvermaidenId)
        {
            Name = "Silvermaiden",
            FlavorText = "The Metalwrought made of silver, the Mistress of the Mirror",
            A = AbilityList.Metalwrought, //TODO: Figure out how to make this ability (and, I suppose, all passives) work
            B = AbilityList.SilverMirror,
        };

        public const int LowGatekeeperId = 11;
        public static readonly MercenaryCard LowGatekeeper = new MercenaryCard(2, 1, 2, 1, LowGatekeeperId)
        {
            Name = "Low Gatekeeper",
            FlavorText = "Just a lowly gatekeeper.",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        public const int DarkEndRaiderId = 12;
        public static readonly MercenaryCard DarkEndRaider = new MercenaryCard(4, 5, 4, 5, DarkEndRaiderId)
        {
            Name = "Dark End Raider",
            FlavorText = "A captain in Almanaught's forces and a brute.",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        public const int WhirlingSpectreId = 13;
        public static readonly MercenaryCard WhirlingSpectre = new MercenaryCard(2, 5, 2, 3, WhirlingSpectreId)
        {
            Name = "Whirling Spectre",
            FlavorText = "Ghosts are just urban legends, right? ...Right?",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        public const int WarpGladiatorId = 14;
        public static readonly MercenaryCard WarpGladiator = new MercenaryCard(5, 5, 2, 3, WarpGladiatorId)
        {
            Name = "Warp Gladiator",
            FlavorText = "Warped are her ideals and thirsty she is for blood.",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        public const int WillBreakerId = 15;
        public static readonly MercenaryCard WillBreaker = new MercenaryCard(3, 3, 7, 4, WillBreakerId)
        {
            Name = "Will Breaker",
            FlavorText = "A lieutenant of Almanaught. His cunning knows no bounds.",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        public const int ShatterBoltId = 16;
        public static readonly MercenaryCard ShatterBolt = new MercenaryCard(4, 10, 3, 6, ShatterBoltId)
        {
            Name = "Shatter Bolt",
            FlavorText = "Made of pure energy, hatred, and confusion.",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        public const int SpireknightId = 17;
        public static readonly MercenaryCard Spireknight = new MercenaryCard(6, 4, 6, 5, SpireknightId)
        {
            Name = "Spireknight",
            FlavorText = "His loyalty knows only one master, and it is not honor.",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        public const int EkethusId = 18;
        public static readonly MercenaryCard Ekethus = new MercenaryCard(10, 10, 10, 10, EkethusId)
        {
            Name = "Ekethus",
            FlavorText = "A guttural roar pierced the darkness, and the Raiders knew they were done for.",
            A = AbilityList.None,
            B = AbilityList.None,
        };

        public const int MemorializedSoulId = 19;
        public static readonly MercenaryCard MemorializedSoul = new MercenaryCard(3, 3, 3, 3, MemorializedSoulId)
        {
            Name = "Memorialized Soul",
            FlavorText = "Gone, but not forgotten. Lost, but still loved.",
            A = AbilityList.None,
            B = AbilityList.None,
            // A: Pure Spirit (Cannot attack. Retaliate: deal AP damage to attacker.)
            // B: Peaceful Rest (Vengeance: Respawn immediately in front of owning player's commander.)
        };

        public const int PeacekeeperAgentId = 20;
        public static readonly MercenaryCard PeacekeeperAgent = new MercenaryCard(2, 4, 4, 3, PeacekeeperAgentId)
        {
            Name = "Peacekeeper Agent",
            FlavorText = "As long as the Pyran Cult was a threat, Forzine would not rest.",
            A = AbilityList.Detain,
            B = AbilityList.Shootout,
        };*/
        #endregion
        /// <summary>
        /// Retrieves the mercenary of the given ID.
        /// </summary>
        /// <param name="id">The id of the mercenary to retrieve.</param>
        /// <returns>The mercenary card related to the id. If it doesn't exist, then null.</returns>
        public static MercenaryCard GetMercenary(int id)
        {
            if (id >= Mercenaries.Count || id < 0)
                return null;
            return Mercenaries[id];
        }
        /// <summary>
        /// Gets the mercenary with the given name.
        /// </summary>
        /// <param name="name">The name of the unit.</param>
        /// <returns>That unit.</returns>
        public static MercenaryCard GetMercenary(string name)
        {
            return GetMercenary(Mercenaries.Find(m => m.Name == name).Id);
        }

        internal static void AddMercenary(MercenaryCard card)
        {
            Mercenaries.Add(card);
        }

        public static List<MercenaryCard> Mercenaries
        {
            get;
            private set;
        } = new List<MercenaryCard>()
        {
            Nothing,
        };
    }
}

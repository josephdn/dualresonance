﻿using System;

namespace DualResonance.Commands
{
    /// <summary>
    /// A command that will attempt to spawn a unit to the board.
    /// </summary>
    public class SpawnCommand : AbstractCommand
    {
        private readonly BoardCoordinate location;
        private readonly int deckPosition;

        public SpawnCommand(
            IPlayer actor, 
            BoardCoordinate location, 
            int deckPosition) : base(actor)
        {
            this.location = location;
            this.deckPosition = deckPosition;
        }

        public override bool Apply(GameSession context, out string message)
        {
            try
            {
                context.Spawn(actor, location, deckPosition);
                message = SuccessMsg;
                return true;
            }
            catch (InvalidDRActionException ioe)
            {
                message = ioe.Message;
                return false;
            }
        }
    }
}

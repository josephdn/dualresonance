﻿using System;

namespace DualResonance.Commands
{
    public class RestCommand : AbstractCommand
    {
        private readonly BoardCoordinate location;

        public RestCommand(IPlayer actor, BoardCoordinate location) : base(actor)
        {
            this.location = location;
        }

        public override bool Apply(GameSession context, out string message)
        {
            try
            {
                context.Rest(actor, location);
                message = SuccessMsg;
                return true;
            }
            catch (InvalidDRActionException ioe)
            {
                message = ioe.Message;
                return false;
            }
        }
    }
}

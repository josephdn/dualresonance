﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DualResonance.Commands
{
    interface IGameCommand
    {
        bool Apply(GameSession context, out string message);
    }
}

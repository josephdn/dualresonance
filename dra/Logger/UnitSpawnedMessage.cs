﻿using DualResonance.Deployables.Units;

namespace DualResonance.Logger
{
    class UnitSpawnedMessage : LogMessage
    {
        public readonly ActiveBase spawn;

        public UnitSpawnedMessage(ActiveBase spawn)
        {
            this.spawn = spawn;
        }

        public override string ToString()
        {
            return $"{spawn.Name} entered play!";
        }
    }
}

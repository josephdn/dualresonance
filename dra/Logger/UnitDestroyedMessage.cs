﻿using DualResonance.Deployables;
using DualResonance.Deployables.Units;

namespace DualResonance.Logger
{
    class UnitDestroyedMessage : LogMessage
    {
        public readonly ActiveBase Target;
        public readonly RemovalCause cause;

        public UnitDestroyedMessage(ActiveBase target, RemovalCause cause)
        {
            Target = target;
            this.cause = cause;
        }

        public override string ToString()
        {
            return $"{Target.Name} was destroyed.";
        }
    }
}

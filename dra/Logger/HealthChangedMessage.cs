﻿using DualResonance.Deployables.Units;

namespace DualResonance.Logger
{
    class HealthChangedMessage : LogMessage
    {
        public readonly int DamageDealt;
        public readonly ActiveBase Target;

        public HealthChangedMessage(int damageDealt, ActiveBase target)
        {
            DamageDealt = damageDealt;
            Target = target;
        }

        public override string ToString()
        {
            return $"{Target.Name} took {DamageDealt} damage.";
        }
    }
}

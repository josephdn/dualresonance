﻿namespace DualResonance
{
    public interface ITick
    {
        /// <summary>
        /// This method should be called at the end of each turn.
        /// </summary>
        void Tick();
    }
}

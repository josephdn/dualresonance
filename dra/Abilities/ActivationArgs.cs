﻿using DualResonance.Deployables.Units;

namespace DualResonance.Abilities
{
    /// <summary>
    /// The base class for ability activation.
    /// </summary>
    public class ActivationArgs
    {
        public ActiveMercenary Other;
        public GameBoard Board;

        public ActivationArgs(ActiveMercenary other, GameBoard board)
        {
            Other = other;
            Board = board;
        }
    }
}

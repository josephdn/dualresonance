﻿namespace DualResonance.Abilities
{
    [System.Flags]
    public enum AbilityTargets
    {
        User,
        Target
    }

    public enum AbilityDamageSource
    {
        AttackPower,
        WillPower,
        Other,
        /// <summary>
        /// A static number
        /// </summary>
        Static
    }

    public enum AbilityVerification
    {
        /// <summary>
        /// Ability can only be used on units of the same team.
        /// </summary>
        SameTeam,
        /// <summary>
        /// Ability can only be used on units of the opposite team.
        /// </summary>
        OtherTeam,
        /// <summary>
        /// Ability can only be used on other units.
        /// </summary>
        Mercenary,
        /// <summary>
        /// Always able to use this ability.
        /// </summary>
        Always,
        /// <summary>
        /// This ability can never be used.
        /// </summary>
        Never
    }
}

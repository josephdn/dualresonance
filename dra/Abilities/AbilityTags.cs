﻿namespace DualResonance.Abilities
{
    /// <summary>
    /// A series of tags that can be applied to an ability.
    /// </summary>
    [System.Flags]
    public enum AbilityTags
    {
        // Notice the pattern here. It goes 1 -> 2 -> 4 -> 8, then adds another
        // zero at the end. When you add a new flag, follow this pattern.
        Barrier                 = 0x0000001,
        Burst                   = 0x0000002,
        Psyche                  = 0x0000004,
        Distort                 = 0x0000008,
        Triage                  = 0x0000010,
        TargetsMerc             = 0x0000020,
        TargetsAlly             = 0x0000040,
        TargetsEnemy            = 0x0000080,
        TargetsAll              = 0x0000100,
        InflictsAilment         = 0x0000200,
        Poisons                 = 0x0000400,
        PoisonsUnit             = 0x0000800,
        PoisonsTarget           = 0x0001000,
        Disables                = 0x0002000,
        DisablesUnit            = 0x0004000,
        DisablesTarget          = 0x0008000,
        Damages                 = 0x0010000,
        DamagesUnit             = 0x0020000,
        DamagesTarget           = 0x0040000,
        Heals                   = 0x0080000,
        HealsUnit               = 0x0100000,
        HealsTarget             = 0x0200000,
        ChangesStats            = 0x0400000,
        ChangesStatsOnUnit      = 0x0800000,
        ChangesStatsOnTarget    = 0x1000000,
    }
}

﻿namespace DualResonance
{
    /// <summary>
    /// Represents the abilities available to a unit.
    /// </summary>
    public enum AbilityId
    {
        /// <summary>
        /// The "attack" ability.
        /// </summary>
        Attack,
        /// <summary>
        /// The A ability.
        /// </summary>
        A,
        /// <summary>
        /// The B ability.
        /// </summary>
        B,
    }
}
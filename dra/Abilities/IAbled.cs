﻿namespace DualResonance.Abilities
{
    /// <summary>
    /// Represents an entity that has two abilities.
    /// </summary>
    public interface IAbled
    {
        Ability A { get; }
        Ability B { get; }
    }
}

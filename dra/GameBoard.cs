﻿using DualResonance.Abilities;
using DualResonance.Deployables;
using DualResonance.Deployables.Units;
using DualResonance.Logger;
using System.Linq;
using System.Collections.Generic;

namespace DualResonance
{
    internal class OutOfBoundsAttackArgs
    {
        public Direction d;
        public BoardCoordinate l;
        public ActiveBase user;
    }

    /// <summary>
    /// Represents thefore GameBoard, where all the action takes place.
    /// </summary>
    public class GameBoard : ITick
    {
        public const int PLAY_AREA_DIMS_X = 5;
        public const int PLAY_AREA_DIMS_Y = 7;

        internal readonly IActive[,] PlayArea = new IActive[PLAY_AREA_DIMS_X, PLAY_AREA_DIMS_Y];

        internal delegate ActiveBase OnOutOfBoundsAttack(object sender, OutOfBoundsAttackArgs args);
        internal event OnOutOfBoundsAttack OutOfBoundsAttack;

        public GameBoard() { }

        /// <summary>
        /// Gets the active object at the queried location.
        /// </summary>
        /// <param name="location">The location to look at.</param>
        /// <returns>The object at the location, or null if there is none.</returns>
        public IActive Query(BoardCoordinate location)
        {
            return PlayArea[location.X, location.Y];
        }

        /// <summary>
        /// Moves 
        /// </summary>
        /// <param name="actor">The player attempting the move.</param>
        /// <param name="location">The location of the mercenary to move.</param>
        /// <param name="direction">The direction to move the mercenary in.</param>
        public void MoveMercenary(IPlayer actor, BoardCoordinate location, BoardCoordinate destination)
        {
            if (!CoordinateIsInBounds(destination))
                throw new InvalidDRActionException("You can't move this unit there - out of bounds.");
            else if (PlayArea[destination.X, destination.Y] != null)
                throw new InvalidDRActionException("You can't move this unit there - something's in the way.");

            PlayArea[destination.X, destination.Y] = PlayArea[location.X, location.Y];
            PlayArea[location.X, location.Y] = null;
            ((ILocatable)PlayArea[destination.X, destination.Y]).Move(destination);
        }

        public void SpawnMercenary(BoardCoordinate location, ActiveBase spawned)
        {
            // Don't allow spawning something on top of another
            if (!IsEmpty(location))
                throw new InvalidDRActionException("Cannot place - there is something in the way.");
            // Don't let anything spawn outside of the field
            else if (!CoordinateIsInBounds(location))
                throw new InvalidDRActionException("The provided board location is out of bounds.");
            // Ensure that there's no more than one of the unit.

            spawned.Remove += Spawned_Remove;
            PlayArea[location.X, location.Y] = spawned;
            if (spawned is AbstractMoveableUnit) (spawned as AbstractMoveableUnit).Location = location;
            Log.Instance.RegisterEvent(new UnitSpawnedMessage(spawned));
        }

        /// <summary>
        /// Causes the unit to enter its Resting state.
        /// </summary>
        /// <param name="actor">The player performing the action.</param>
        /// <param name="location">The location of the unit to be rested.</param>
        public void Rest(IPlayer actor, BoardCoordinate location)
        {
            if (!CoordinateIsInBounds(location))
                throw new InvalidDRActionException("This isn't available.");
            else if (IsEmpty(location))
                throw new InvalidDRActionException("Can't do this - nothing's here");
            else if (!CoordinateIsInBounds(location))
                throw new InvalidDRActionException("The provided board location is out of bounds.");
            else if (((ActiveMercenary)PlayArea[location.X, location.Y]).OwnerPlayerId != actor.PlayerId)
                throw new InvalidDRActionException("This isn't under your command");

            PlayArea[location.X, location.Y].Rest();
        }

        private void Spawned_Remove(object sender, DeathEventArgs args)
        {
            var unit = PlayArea[args.location.X, args.location.Y];
            // This will eventually need to do more, but for now, this is fine.
            PlayArea[args.location.X, args.location.Y] = null;
            if (unit.A is PassiveAbility && ((PassiveAbility)unit.A).Trigger == AbilityTrigger.Destroyed && unit is ActiveMercenary merc)
                ((PassiveAbility)unit.A).Activate((ActiveMercenary)unit, new ActivationArgs(null, this));            
        }

        /// <summary>
        /// Checks if the given position is belongs to a team.
        /// </summary>
        /// <param name="team">The team to check the board with.</param>
        /// <param name="location">The location to check.</param>
        /// <returns>True if the team can spawn a unit on that tile.</returns>
        public bool EnsureSpawnIsWithinTeamBounds(Teams team, BoardCoordinate location)
        {
            int rowBound = (team == Teams.Alpha ? PLAY_AREA_DIMS_Y / 2 * 2 - 1 : PLAY_AREA_DIMS_Y / 2 - 2);

            if (team == Teams.Alpha)
                return rowBound <= location.Y;
            else
                return rowBound >= location.Y;
        }

        /// <summary>
        /// Checks if the board position is within the player's available spawning range - or their part of the field.
        /// </summary>
        /// <param name="player">The player to check against.</param>
        /// <param name="location">The location to check.</param>
        /// <returns>True if the location is within the player's available spawn space.</returns>
        public bool EnsureSpawnIsWithinPlayerBounds(IPlayer player, BoardCoordinate location)
        {
            // Alpha: Players. Bottom "Half".
            // Beta: Enemies. Top "Half".
            return EnsureSpawnIsWithinTeamBounds(player.Team, location);
        }

        /// <summary>
        /// Ensures that a valid interaction can occur and gets the user and the target of the interaction.
        /// </summary>
        /// <param name="location">The location to query the user at.</param>
        /// <param name="d">The direction in which to direct the interaction.</param>
        /// <param name="user">The user of the interaction (should be null).</param>
        /// <param name="target">The target of the interaction (should be null).</param>
        private void PrepareInteraction(
            BoardCoordinate location,
            Direction d,
            out IActive user,
            out IActive target)
        {
            if (IsEmpty(location))
                throw new InvalidDRActionException("No mercenary is at this location.");
            BoardCoordinate targetLocation = location + BoardCoordinate.FromDirection(d);
            if (CoordinateIsInBounds(targetLocation) && IsEmpty(targetLocation))
                throw new InvalidDRActionException("There's nothing in that direction.");

            user = PlayArea[location.X, location.Y];
            if (!CoordinateIsInBounds(targetLocation))
            {
                OutOfBoundsAttackArgs args = new OutOfBoundsAttackArgs()
                {
                    l = location,
                    d = d,
                    user = (ActiveBase)user,
                };
                target = OutOfBoundsAttack(this, args);
            }
            else
                target = PlayArea[targetLocation.X, targetLocation.Y];
            if (target == null)
                throw new InvalidDRActionException("There's nothing in that direction.");
        }

        /// <summary>
        /// Causes the unit at the location to attack in the given direction.
        /// </summary>
        /// <param name="location">The position of the unit.</param>
        /// <param name="d">The direction to attack in.</param>
        public void InvokeAttack(BoardCoordinate location, Direction d)
        {
            PrepareInteraction(location, d, out IActive user, out IActive target);
            ((ActiveMercenary)user).Attack((ActiveBase)target);
        }

        /// <summary>
        /// Causes the mercenary at the location to use their ability.
        /// </summary>
        /// <param name="location">The location of the mercenery using the ability.</param>
        /// <param name="d">The direction the target lies in. Assuming a range of one.</param>
        /// <param name="abilityPtr">The ability to use. 'a' is A, anything else is B.</param>
        public void InvokeAbility(BoardCoordinate location, Direction d, AbilityId ability)
        {
            PrepareInteraction(location, d, out IActive user, out IActive target);

            if (!(user as ActiveMercenary).InvokeAbility((ActiveBase)target, ability))
                throw new InvalidDRActionException("Invalid target.");
        }

        /// <summary>
        /// Ensures that the given coordinate is within the bounds of the game board.
        /// </summary>
        /// <param name="location">The location to check against.</param>
        /// <returns>True if the coordinate is within 0 and PLAY_AREA_DIMS in both directions.</returns>
        public bool CoordinateIsInBounds(BoardCoordinate location)
        {
            return location.X >= 0 && location.X < PLAY_AREA_DIMS_X && location.Y >= 0 && location.Y < PLAY_AREA_DIMS_Y;
        }

        /// <summary>
        /// Checks if there's a unit at the location.
        /// </summary>
        /// <param name="location">The location to check.</param>
        /// <returns>True if there is nothing at that location.</returns>
        public bool IsEmpty(BoardCoordinate location)
        {
            return PlayArea[location.X, location.Y] == null;
        }

        /// <summary>
        /// Causes the board itself to tick.
        /// </summary>
        public void Tick()
        {

        }

        /// <summary>
        /// Retrieves all the units belonging to the player on the field.
        /// </summary>
        /// <param name="player">The owner of the units to find.</param>
        /// <returns>A collection of all mercs pertaining to the player.</returns>
        public IEnumerable<ActiveMercenary> GetPlayersActiveMercs(IPlayer player)
        {
            return GetPlayersActiveMercs(player.PlayerId);
        }

        private IEnumerable<ActiveMercenary> GetPlayersActiveMercs(int playerId)
        {
            return from ActiveMercenary merc in PlayArea
                   where merc != null
                   && merc.OwnerPlayerId == playerId
                   select merc as ActiveMercenary;
        }

        public IEnumerable<ActiveMercenary> GetPlayersMercs(IPlayer player)
        {
            return from ActiveMercenary merc in PlayArea
                   where merc != null
                   && merc.OwnerPlayerId == player.PlayerId
                   select merc as ActiveMercenary;
        }

        /// <summary>
        /// Retrieves all mercs in the cardinal directions of the chosen location.
        /// </summary>
        /// <param name="location">The location of the merc.</param>
        /// <param name="player">If not null, will pull all mercs of the specified team.</param>
        /// <param name="sameTeam">If player is not null, </param>
        /// <param name="includeAtLocation">Whether to include the merc at the location or not. Defaults to false.</param>
        /// <returns>All mercs with the specified conditions met.</returns>
        public IEnumerable<ActiveMercenary> GetMercsInCardinalDirections( 
            BoardCoordinate location,
            IPlayer player = null,
            bool sameTeam = true,
            bool includeAtLocation = false)
        {
            var nonNulls = from ActiveMercenary merc in PlayArea
                           where merc != null
                           select merc as ActiveMercenary;
            return from ActiveMercenary merc in nonNulls
                   where (merc.Location.X == location.X || merc.Location.Y == location.Y)
                   && includeAtLocation ? merc.Location != location : true
                   && player != null ? 
                        (sameTeam ? merc.OwnerPlayerId == player.PlayerId :
                         merc.OwnerPlayerId != player.PlayerId) : true
                   select merc as ActiveMercenary;
        }

        /// <summary>
        /// Finds all instances of the unit on the board.
        /// </summary>
        /// <param name="mercId">The unit's id.</param>
        /// <returns></returns>
        public IEnumerable<ActiveMercenary> GetMercsWithId(int mercId)
        {
            return from ActiveMercenary merc in PlayArea
                   where merc != null
                   && merc.MercenaryId == mercId
                   select merc as ActiveMercenary;
        }

        /// <summary>
        /// Finds all units on the specified team.
        /// </summary>
        /// <param name="team">The team to search for.</param>
        /// <returns>A list of all units on the given team.</returns>
        public IEnumerable<ActiveMercenary> GetMercsOnTeam(Teams team)
        {
            return from ActiveMercenary merc in PlayArea
                   where merc != null
                   && merc.Team == team
                   select merc as ActiveMercenary;
        }
    }
}

﻿using System;

namespace DualResonance
{
    /// <summary>
    /// Represents an invalid action taken within a Dual Resonance game.
    /// </summary>
    public class InvalidDRActionException : Exception
    {
        public InvalidDRActionException(string message) : base(message)
        {

        }
    }
}

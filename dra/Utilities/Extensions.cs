﻿using System;

namespace DualResonance.Utilities
{
    public static class Extensions
    {
        /// <summary>
        /// Removes the element from the array at the specific index.
        /// </summary>
        /// <typeparam name="T">The array type.</typeparam>
        /// <param name="source">The array having the element removed from it.</param>
        /// <param name="index">The index to remove.</param>
        /// <returns>The new array without the element. Its size is retained.</returns>
        /// <see cref="https://stackoverflow.com/a/457501/4500719"/>
        public static T[] RemoveAt<T>(this T[] source, int index)
        {
            T[] dest = new T[source.Length]; // Unlike in the original SO answer, I don't want the array to be shrunk.
            if (index > 0)
                Array.Copy(source, 0, dest, 0, index);

            if (index < source.Length - 1)
                Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

            return dest;
        }
    }
}

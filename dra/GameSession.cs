﻿using DualResonance.Deployables;
using DualResonance.Deployables.Units;
using DualResonance.Logger;
using DualResonance.Sessions;
using System;
using System.Collections.Generic;

namespace DualResonance
{
    public class GameSession : ITick
    {
        internal GameBoard Board = new GameBoard();

        /// <summary>
        /// The name of the game board.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// If the game has been fully initialized yet.
        /// </summary>
        public bool Initialized { get; private set; } = false;

        #region Player Data
        /// <summary>
        /// The total amount of players in the session.
        /// </summary>
        public int PlayerCount { get => AIPlayerCount + HumanPlayerCount; }
        /// <summary>
        /// The amount of AI players in this session.
        /// </summary>
        public int AIPlayerCount { get; private set; }
        /// <summary>
        /// The amount of human players in this session.
        /// </summary>
        public int HumanPlayerCount { get; private set; }

        private int aTeamIndex = 0;
        private List<IPlayer> AlphaTeam = new List<IPlayer>();
        private int bTeamIndex = 0;
        private List<IPlayer> BetaTeam = new List<IPlayer>();
        private List<IPlayer> players = new List<IPlayer>();

        /// <summary>
        /// The currently active player.
        /// </summary>
        public IPlayer ActivePlayer { get; private set; }

        /// <summary>
        /// A list of all active commanders. Their index corresponds to the player id.
        /// </summary>
        internal List<ActiveCommander> PlayerCommanders { get; private set; }
            = new List<ActiveCommander>();

        #endregion Player Data

        #region Win Conditions
        private Teams victoriousTeam = Teams.Neutral;
        /// <summary>
        /// The winning team (starts out neutral).
        /// </summary>
        public Teams VictoriousTeam
        {
            get => victoriousTeam;
            private set
            {
                victoriousTeam = value;
                Victory(this, value);
            }
        }

        private List<Condition> alphaWinConditions
            = new List<Condition>();
        private List<Condition> betaWinConditions
            = new List<Condition>();

        public Condition[] AlphaWinConditions => alphaWinConditions.ToArray();

        /// <summary>
        /// Triggered once a victorious team is decided.
        /// </summary>
        /// <param name="sender">This object that has decided victory.</param>
        /// <param name="victoriousTeam">The team that's been decided as the victor.</param>
        public delegate void OnTeamVictory(object sender, Teams victoriousTeam);
        /// <summary>
        /// This event is called once a victorious team is decided.
        /// </summary>
        public event OnTeamVictory Victory;
        #endregion Win Conditions

        /// <summary>
        /// Creates a new game session.
        /// </summary>
        public GameSession() { }

        /// <summary>
        /// Registers the win condition to the session.
        /// </summary>
        /// <param name="condition">The win condition.</param>
        public void AddWinCondtion(Condition condition)
        {
            switch (condition.Winner)
            {
                case Teams.Alpha:
                    alphaWinConditions.Add(condition);
                    break;
                case Teams.Beta:
                    betaWinConditions.Add(condition);
                    break;
                default:
                    throw new InvalidDRActionException("Invalid team.");
            }
        }

        private ActiveBase Board_OutOfBoundsAttack(object sender, OutOfBoundsAttackArgs args)
        {
            if (args.d == Direction.South)
                return PlayerCommanders[0];
            else if (args.d == Direction.North)
                return PlayerCommanders[1];
            else return null;
        }

        /// <summary>
        /// Gets the player with the specified ID.
        /// </summary>
        /// <param name="playerId">The id of the player to get.</param>
        /// <returns>A player entity.</returns>
        public IPlayer GetPlayer(int playerId)
        {
            return players[playerId];
        }

        /// <summary>
        /// Adds players to each team.
        /// </summary>
        /// <param name="alphaTeamPlayers">The players to add to the alpha team.</param>
        /// <param name="betaTeamPlayers">The players to add to the beta team.</param>
        private void SetPlayers(
            List<IPlayer> alphaTeamPlayers,
            List<IPlayer> betaTeamPlayers
            )
        {
            foreach (var player in alphaTeamPlayers)
            {
                player.Team = Teams.Alpha;
                AddPlayer(player);
            }

            foreach (var player in betaTeamPlayers)
            {
                player.Team = Teams.Beta;
                AddPlayer(player);
            }
        }

        private void AssertPlayerTurn(IPlayer player)
        {
            if (ActivePlayer.PlayerId != player.PlayerId)
                throw new InvalidDRActionException("It is not your turn.");
        }

        /// <summary>
        /// Queries the board for the unit at the specified location.
        /// </summary>
        /// <param name="coordinate">The coordinate on the board to query.</param>
        /// <returns>Null if nothing is there, or the unit at the location.</returns>
        public ActiveBase Query(BoardCoordinate coordinate)
        {
            return (ActiveBase)Board.Query(coordinate);
        }

        /// <summary>
        /// Checks if the given coordinate is within the board.
        /// </summary>
        /// <param name="coordinate">The coordinate to check.</param>
        /// <returns>True if the coordinate is in bounds.</returns>
        public bool IsInBounds(BoardCoordinate coordinate)
        {
            return Board.CoordinateIsInBounds(coordinate);
        }

        /// <summary>
        /// Checks if the coordinate is within the player's spawning range.
        /// </summary>
        /// <param name="team">The team of the player.</param>
        /// <param name="coordinate">The location to check.</param>
        /// <returns>True if the location is within bounds.</returns>
        public bool IsInTeamBounds(Teams team, BoardCoordinate coordinate)
        {
            return Board.EnsureSpawnIsWithinTeamBounds(team, coordinate);
        }

        /// <summary>
        /// Adds the player to the game. The player must have their deck set beforehand.
        /// </summary>
        /// <exception cref="InvalidDRActionException">Thrown if team is neutral or the game has already started.</exception>
        /// <exception cref="ArgumentException">Thrown if the player doesn't have a deck set.</exception>
        /// <param name="player">The player to add to the game.</param>
        public void AddPlayer(IPlayer player)
        {
            if (Initialized)
                throw new InvalidDRActionException("The game has already started. No more players can be added.");
            if (player.CurrentDeck == null)
                throw new ArgumentException("The player's deck cannot be null.");

            if (player.Team == Teams.Alpha)
                AlphaTeam.Add(player);
            else if (player.Team == Teams.Beta)
                BetaTeam.Add(player);
            else
                throw new InvalidDRActionException("Neutral players are not supported.");

            player.PlayerId = PlayerCount;
            players.Add(player);
            if (player is AIPlayer)
                AIPlayerCount++;
            else
                HumanPlayerCount++;
            if (player.CurrentDeck.Commander != null)
            {
                var commander = new ActiveCommander(player.CurrentDeck.Commander, player.Team);
                commander.Tick();
                PlayerCommanders.Add(commander);
            }
            else
            {
                PlayerCommanders.Add(null);
            }
            Log.Instance.RegisterEvent(new PlayerAddedMessage((Player)player));
        }

        /// <summary>
        /// Causes the Session to be fully initialized.
        /// </summary>
        public void Initialize()
        {
            if (AlphaTeam.Count == 0)
                throw new InvalidDRActionException("No players have been added to alpha team. Cannot initialize game.");
            ActivePlayer = AlphaTeam[0];
            // Makes sure that the board is properly set up. This might belong elsewhere.
            Board.OutOfBoundsAttack += Board_OutOfBoundsAttack;
            Initialized = true;
        }

        /// <summary>
        /// Causes the unit at the specified location to attack in the given direction.
        /// </summary>
        /// <param name="actor">The player invoking the attack.</param>
        /// <param name="location">The location of the attacking unit.</param>
        /// <param name="d">The direction the unit should attack in.</param>
        public void InvokeAttack(
            IPlayer actor, 
            BoardCoordinate location,
            Direction d)
        {
            AssertPlayerTurn(actor);
            Board.InvokeAttack(location, d);
            PerformAllEndActionChecks();
        }

        /// <summary>
        /// Causes the unit at the location to use its ability in the specified direction.
        /// </summary>
        /// <param name="actor">The player invoking the ability.</param>
        /// <param name="location">The location of the unit using the ability.</param>
        /// <param name="d">The direction the ability should be cast in.</param>
        /// <param name="ability">The ability that should be used.</param>
        public void InvokeAbility(
            IPlayer actor,
            BoardCoordinate location,
            Direction d,
            AbilityId ability)
        {
            AssertPlayerTurn(actor);

            var user = Board.PlayArea[location.X, location.Y];

            var cast = ability == AbilityId.A ? user.A : user.B;
            var commander = PlayerCommanders[ActivePlayer.PlayerId];
            int resonanceCost = cast.GetTotalCost(commander.GeneratedResonances);
            if (commander.CurrentResonance < resonanceCost)
                throw new InvalidDRActionException("Not enough resonance");
            // If the ability fails here, an exception will be thrown. Don't
            // catch it.
            Board.InvokeAbility(location, d, ability);

            commander.CurrentResonance -= resonanceCost;

            PerformAllEndActionChecks();
        }

        /// <summary>
        /// Moves the unit at the location in the desired direction.
        /// </summary>
        /// <param name="actor">The player making this action.</param>
        /// <param name="origin">The location of the unit to be moved.</param>
        /// <param name="direction">The direction to move the unit in.</param>
        public void MoveUnit(
            IPlayer actor, 
            BoardCoordinate origin, 
            BoardCoordinate destination)
        {
            IActive mercenary;
            if ((mercenary = Board.Query(origin)) == null)
                throw new InvalidDRActionException("There is no mercenary at the given location.");
            else if (!(mercenary is ActiveMercenary) || ((ActiveMercenary)mercenary).OwnerPlayerId != actor.PlayerId)
                throw new InvalidDRActionException("You can't command this object.");
            else if (!(((ActiveMercenary)mercenary).UnitState is Deployables.Units.States.SpryUnitState))
                throw new InvalidDRActionException("This unit has already moved.");
            else if (origin.DistanceTo(destination) > ActiveBase.MAX_MOVEMENT)
                throw new InvalidDRActionException($"You can't move units more than {ActiveBase.MAX_MOVEMENT} spaces.");

            Board.MoveMercenary(actor, origin, destination);
        }

        /// <summary>
        /// Places a unit on the field.
        /// </summary>
        /// <param name="actor">The player attempting the action.</param>
        /// <param name="location">The position at which to place the merc.</param>
        /// <param name="deckPosition">The position in the player's deck to pull the mercenary information from.</param>
        public void Spawn(
            IPlayer actor, 
            BoardCoordinate location, 
            int deckPosition)
        {
            if (Initialized) AssertPlayerTurn(actor);
            if (deckPosition < 0 || deckPosition > DeckInfo.MERCENARY_COUNT)
                throw new InvalidDRActionException("Unidentified mercenary.");
            // This shouldn't ever happen, but this will help make sure that it doesn't.
            else if (actor.CurrentDeck == null)
                throw new NullReferenceException("The player's deck has yet to be set.");
            else if (!Board.EnsureSpawnIsWithinPlayerBounds(actor, location))
                throw new InvalidDRActionException("Cannot spawn here - out of your spawn range.");

            var merc = actor.CurrentDeck.GetMercenary(deckPosition);
            if (merc == null)
                throw new InvalidDRActionException("Invalid deck position");
            if (Initialized && PlayerCommanders[actor.PlayerId].CurrentGold < merc.GoldCost)
                throw new InvalidDRActionException("Cannot spawn this - not enough gold.");

            PlayerCommanders[actor.PlayerId].CurrentGold -= merc.GoldCost;

            foreach (var m in Board.GetMercsWithId(merc.Id))
            {
                if (m.OwnerPlayerId == actor.PlayerId)
                    throw new InvalidDRActionException("You already have this unit in play.");
            }

            ActiveMercenary spawned = new ActiveMercenary(merc, actor.Team)
            {
                OwnerPlayerId = actor.PlayerId,
                Location = location,
            };

            Board.SpawnMercenary(location, spawned);
        }

        /// <summary>
        /// Places an existing unit at a location on the battlefield.
        /// </summary>
        /// <param name="spawned">The created unit.</param>
        /// <param name="location">The location the unit should be placed at.</param>
        public void Spawn(ActiveBase spawned, BoardCoordinate location)
        {
            Board.SpawnMercenary(location, spawned);
        }

        /// <summary>
        /// Causes the unit at the location to rest.
        /// </summary>
        /// <param name="actor">The player invoking the command.</param>
        /// <param name="location">The location of the unit to have rest.</param>
        public void Rest(IPlayer actor, BoardCoordinate location)
        {
            if (ActivePlayer.PlayerId != actor.PlayerId)
                throw new InvalidDRActionException("It's not your turn.");
            Board.Rest(actor, location);
            PerformAllEndActionChecks();
        }

        /// <summary>
        /// Gets the commander for the given player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>The commander associated with the player.</returns>
        public ActiveCommander GetPlayerCommander(IPlayer player) =>
            PlayerCommanders[player.PlayerId];

        /// <summary>
        /// Gets the commander for the player with the specified id.
        /// </summary>
        /// <param name="playerId">The id of the player.</param>
        /// <returns>The commander pertaining to the player id.</returns>
        public ActiveCommander GetPlayerCommander(int playerId) => 
            PlayerCommanders[playerId];

        /// <summary>
        /// Performs all checks that must be performed at the end of an action.
        /// </summary>
        /// <returns>True if the player's turn has ended.</returns>
        private bool PerformAllEndActionChecks()
        {
            // If the previous action resulted in a victory, we'll want to check that.
            CheckVictoryConditions();
            foreach (ActiveMercenary merc in Board.GetPlayersActiveMercs(ActivePlayer))
            {
                if (merc.CanAct)
                {
                    return false;
                }
            }
            Tick();
            return true;
        }

        /// <summary>
        /// Ends the current player's turn.
        /// </summary>
        public void EndTurn()
        {
            // By having the mercs rest themselves and not calling this class's
            // rest method, we save time. If we did that, the call to 
            // PerformAllEndActionChecks() would cause this method to be O(n^2)
            // as opposed to O(n).
            foreach (var m in Board.GetPlayersMercs(ActivePlayer))
                m.Rest();
            Tick();
        }

        /// <summary>
        /// Causes the game state to advance by a portion of a turn.
        /// 
        /// Causes each mercenary on the current player's team to Tick,
        /// and advances to the next player.
        /// </summary>
        public void Tick()
        {
            Board.Tick();

            var mercs = Board.GetPlayersMercs(ActivePlayer);
            foreach (var merc in mercs)
            {
                merc.Tick();
            }

            if (ActivePlayer.Team == Teams.Alpha)
            {
                bTeamIndex = (++bTeamIndex) % BetaTeam.Count;
                ActivePlayer = BetaTeam[bTeamIndex];
            }
            else
            {
                aTeamIndex = (++aTeamIndex) % AlphaTeam.Count;
                ActivePlayer = AlphaTeam[aTeamIndex];
            }

            if (PlayerCommanders[ActivePlayer.PlayerId] != null)
                PlayerCommanders[ActivePlayer.PlayerId].Tick();
            // We do the check here in case the conditions have been met as a result of
            // all the ticking.
            CheckVictoryConditions();
        }

        /// <summary>
        /// Checks the current conditions to see if victory for a team has been met.
        /// </summary>
        private void CheckVictoryConditions()
        {
            bool alphaWins = false;
            foreach (var condition in alphaWinConditions)
                if (!condition.ConditionMet(this))
                {
                    alphaWins = false;
                    break;
                }
                else alphaWins = true;

            if (alphaWins) Victory(this, Teams.Alpha);

            foreach (var condition in betaWinConditions)
                if (condition.ConditionMet(this))
                    Victory(this, condition.Winner);
        }

        /// <summary>
        /// Determines if the team has the specific unit in play.
        /// </summary>
        /// <param name="team">The team to check.</param>
        /// <param name="uid">The id of the unit.</param>
        /// <returns>True if the team has the unit in play.</returns>
        public bool UnitInPlay(Teams team, int uid)
        {
            var units = Board.GetMercsWithId(uid);
            foreach (var unit in units)
            {
                if (unit.Team == team)
                    return true;
            }
            return false;
        }
    }
}

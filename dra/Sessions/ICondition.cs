﻿namespace DualResonance.Sessions
{
    /// <summary>
    /// Represents a simple condition on the game board. 
    /// </summary>
    interface ICondition
    {
        /// <summary>
        /// If the condition normally checked for should be inverted.
        /// </summary>
        bool InvertCondition { get; }
        /// <summary>
        /// Determines if the condition has been met.
        /// </summary>
        /// <param name="session">The session to check for.</param>
        /// <returns>True if conditions have been met; otherwise, false.</returns>
        bool ConditionMet(GameSession session);
    }
}

﻿using DualResonance.Deployables.Cards;
using System;
using System.Linq;

namespace DualResonance.Sessions
{
    public sealed class UnitExistsCondition : Condition
    {
        readonly int unitId;
        readonly Teams unitTeam;
        readonly MercenaryCard targetCard;
        public UnitExistsCondition(Teams unitsTeam, int unitId, Teams winningTeam, bool invert) : base(winningTeam, invert)
        {
            unitTeam = unitsTeam;
            this.unitId = unitId;
            try
            {
                targetCard = MercenaryCards.GetMercenary(unitId);
            }
            catch (Exception)
            {
                throw new InvalidOperationException("The unit id doesn't exist.");
            }
        }
        /// <summary>
        /// Checks if the condition has been met.
        /// </summary>
        /// <param name="session">The session this condition belongs to.</param>
        /// <returns>True if the unit is no longer on the board.</returns>
        protected override bool SpecificCheck(GameSession session)
        {
            var mercs = session.Board.GetMercsWithId(unitId);
            if (mercs.Count() == 0) return false;
            foreach (var merc in mercs)
            {
                if (merc.Team == unitTeam)
                    return true;
            }
            return false;
        }

        public override string ToString()
        {
            return $"Defeat the enemy {targetCard.Name}.";
        }
    }
}

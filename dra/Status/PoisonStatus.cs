﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DualResonance.Deployables.Units;

namespace DualResonance.Status
{
    public class PoisonStatus : StatusBase, IAilmentStatus
    {
        public PoisonStatus(int duration, int severity, StatusPrecedence precedence) : base(duration, severity, precedence)
        {

        }

        public override void Apply(ActiveMercenary mercenary)
        {
            mercenary.DecrementHealthPoints(Severity);
        }

        public override string ToString()
        {
            if (Duration == -1)
                return $"Poison {Severity}";
            else if (Duration != 1)
                return $"Poison {Severity} ({Duration} turns)";
            else
                return $"Posion {Severity} (1 turn)";
        }
    }
}

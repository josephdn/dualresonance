﻿namespace DualResonance.Status
{
    class StatModifier : ITick
    {
        public int Duration { get; private set; }

        /// <summary>
        /// Creates a stat modification.
        /// </summary>
        /// <param name="duration">The duration of the stat modification.</param>
        /// <param name="APMod">How much to modify the AP stat.</param>
        /// <param name="WPMod">How much to modify the WP stat.</param>
        public StatModifier(int APMod, int WPMod, int duration)
        {
            this.APMod = APMod;
            this.WPMod = WPMod;
            Duration = duration;
        }

        /// <summary>
        /// The change in Attack Power.
        /// </summary>
        public readonly int APMod;
        /// <summary>
        /// The change in Will Power.
        /// </summary>
        public readonly int WPMod;

        public void Tick()
        {
            if (Duration > 0)
                Duration--;
            else if (Duration == 0)
                return;
            // Negative duration lasts forever.
        }

        /// <summary>
        /// Gets the total modification between two stats.
        /// </summary>
        /// <param name="left">The left hand stat modifier.</param>
        /// <param name="right">The right hand stat modifier.</param>
        /// <returns>The total stat modifications.</returns>
        public static StatModifier operator+(StatModifier left, StatModifier right)
        {
            return new StatModifier(left.APMod + right.APMod, left.WPMod + right.WPMod, -1);
        }

        public static StatModifier operator-(StatModifier left, StatModifier right)
        {
            return new StatModifier(left.APMod - right.APMod, left.WPMod - right.WPMod, - 1);
        }
    }
}

﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DualResonance.Graphics.UI
{
    /// <summary>
    /// A textbox element that disappears after time.
    /// </summary>
    public class FadingTextBox : TemporaryElement
    {
        Vector2 stringDim;
        private SpriteFont font;
        private readonly TextAlignment alignment;
        private string text;
        /// <summary>
        /// The text to be displayed in the textbox.
        /// </summary>
        public string Text
        {
            get => text;
            set
            {
                text = value;
                stringDim = font.MeasureString(Text);
            }
        }
        /// <summary>
        /// The current fading value.
        /// </summary>
        float alpha = 1;
        Color baseColor = Color.Black;

        private float fadeInterval = 1;
        /// <summary>
        /// When to start fading the text, as a percentage of the lifespan.
        /// 
        /// For example, a value of 0 tells it to begin fading immediately, while a value of
        /// 1 tells it to not fade at all. 1 is the default value.
        /// </summary>
        public float FadeInterval
        {
            get => fadeInterval;
            set
            {
                if (value > 1 || value < 0)
                    throw new ArgumentOutOfRangeException("FadeInterval must be a value between 0 and 1.");
                fadeInterval = value;
            }
        }

        public FadingTextBox(Vector2 position, Vector2 scale, int lifespan, string text, SpriteFont font) : this(position, scale, lifespan, text, font, TextAlignment.Center, 1) { }

        /// <summary>
        /// Creates a FadingTextBox.
        /// </summary>
        /// <param name="position">The position in the window to draw this element at.</param>
        /// <param name="scale">The scale used to modify this element with.</param>
        /// <param name="lifespan">How long the element should live, in milliseconds.</param>
        /// <param name="text">The text to be displayed.</param>
        /// <param name="font">The font to use for this element.</param>
        /// <param name="alignment">How to align the text.</param>
        /// <param name="fadeAtPercent">When to start making the text fade.</param>
        public FadingTextBox(Vector2 position, Vector2 scale, int lifespan, string text, SpriteFont font, TextAlignment alignment, float fadeAtPercent) : base(position, scale, lifespan)
        {
            this.font = font;
            Text = text;
            this.alignment = alignment;
            stringDim = font.MeasureString(Text);
            FadeInterval = fadeAtPercent;
        }

        public override void Draw(GameTime gameTime)
        {
            float stringWidth = stringDim.X;
            // Figure out the position that the string should be written to.
            Vector2 startPoint = position;
            switch (alignment)
            {
                case TextAlignment.Center:
                    startPoint.X = position.X - stringWidth / 2;
                    break;
                case TextAlignment.Right:
                    startPoint.X = position.X - stringWidth;
                    break;
                case TextAlignment.Left:
                default:
                    break;
            }
            AssetManager am = AssetManager.Instance;
            am.WriteString(Text, startPoint, new Color(baseColor, alpha));
        }

        protected override void InternalUpdate(GameTime gameTime)
        {
            if (FadeInterval != 1)
            {
                float percentageElapsed = (float)Lifetime / Lifespan;
                if (percentageElapsed >= FadeInterval)
                {
                    float percentageLeft = 1 - percentageElapsed;
                    float fadeStart = Lifespan * FadeInterval;
                    float fadeLifespan = Lifespan - fadeStart;
                    float fadeLifetime = Lifetime - fadeStart;
                    alpha = 1 - (fadeLifetime / fadeLifespan);
                }
            }
        }
    }
}

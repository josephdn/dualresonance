﻿using Microsoft.Xna.Framework;

namespace DualResonance.Graphics.UI
{
    public class ExpireEventArgs
    {
        /// <summary>
        /// The total amount of time the element was active.
        /// </summary>
        public readonly int TimeAlive;

        public ExpireEventArgs(int timeAlive)
        {
            TimeAlive = timeAlive;
        }
    }

    /// <summary>
    /// Represents an Element that has some kind of Lifespan.
    /// </summary>
    public abstract class TemporaryElement : UIElement
    {
        private bool expired;
        /// <summary>
        /// If this item has expired or not.
        /// </summary>
        public bool Expired
        {
            get => expired;
            set
            {
                expired = value;
                if (value)
                    Expire(this, new ExpireEventArgs(lifetime));
            }
        }

        public delegate void ExpireHandler(object sender, ExpireEventArgs e);

        public event ExpireHandler Expire;

        /// <summary>
        /// The amount of time the element should exist, in milliseconds.
        /// </summary>
        public readonly int Lifespan;

        private int lifetime;
        /// <summary>
        /// The amount of time the Element has existed, in milliseconds.
        /// </summary>
        public int Lifetime
        {
            get => lifetime;
            set
            {
                lifetime = value;
                Expired = lifetime > Lifespan;
            }
        }

        /// <summary>
        /// Creates a new TemporaryElement.
        /// </summary>
        /// <param name="lifespan">The amount of time (in milliseconds) that the element should exist.</param>
        public TemporaryElement(Vector2 position, Vector2 scale, int lifespan) : base(position, scale)
        {
            Lifespan = lifespan;
        }

        protected abstract void InternalUpdate(GameTime gameTime);
        /// <summary>
        /// Updates the element. If it has expired, then no updates occur.
        /// </summary>
        /// <param name="gameTime">The time that's elapsed since the last update.</param>
        public sealed override void Update(GameTime gameTime)
        {
            if (Expired) return;
            Lifetime += gameTime.ElapsedGameTime.Milliseconds;
            InternalUpdate(gameTime);
        }

        /// <summary>
        /// Restarts the countdown until the element expires.
        /// </summary>
        /// <param name="gameTime">The amount of time elapsed since the last update.</param>
        public void Restart(GameTime gameTime)
        {
            Lifetime = 0;
        }
    }
}

﻿namespace DualResonance.Graphics.UI
{
    /// <summary>
    /// The alignment of text in a text-based element.
    /// </summary>
    public enum TextAlignment
    {
        // Dead center.
        Center = 0b0000,
        // Left and center.
        Left = 0b0001,
        // Right and center.
        Right = 0b0010,
        // Top and center.
        Top = 0b0100,
        // Top and left.
        TopLeft = 0b0101,
        // Top and right.
        TopRight = 0b0110,
        // Bottom and center.
        Bottom = 0b1000,
        // Bottom and left.
        BottomLeft = 0b1001,
        // Bottom and right.
        BottomRight = 0b1010,
        // Use this to determine the verticality of an element.
        Verticality = 0b1100,
        // Use this to determine the horizontality of an element.
        Horizontality = 0b0011,
    }
}

﻿using Microsoft.Xna.Framework;

namespace DualResonance.Graphics
{
    class Camera3D
    {
        public Vector3 LookAt = Vector3.Zero;
        public Vector3 LookFrom = Vector3.UnitZ;
        public Vector3 Up = Vector3.Up;

        public float RotationY { get; set; } = MathHelper.Pi;
        public float RotationX { get; set; }

        public Matrix ViewMatrix
        {
            get => Matrix.CreateLookAt(LookFrom, LookAt, Up);
        }

        public Matrix Rotation
        {
            get
            {
                var rotationY = Matrix.CreateRotationY(RotationY);
                var rotated = rotationY * Matrix.CreateRotationX(RotationX);
                return rotated;
            }
        }

        public Matrix Transform
        {
            get
            {
                var rotation = Matrix.CreateRotationY(RotationY);
                rotation *= Matrix.CreateRotationX(RotationX);
                return rotation * Matrix.CreateTranslation(LookFrom);
            }
        }

        public Camera3D() { }

        /// <summary>
        /// Increments rotation about the Y axis.
        /// </summary>
        /// <param name="radians">The new amount of rotation to handle.</param>
        public void RotateY(float degrees)
        {
            RotationY = (RotationY + MathHelper.ToRadians(degrees)) % MathHelper.TwoPi;
        }

        /// <summary>
        /// Increments rotation about the X axis.
        /// </summary>
        /// <param name="radians">The new amount of rotation to handle.</param>
        public void RotateX(float degrees)
        {
            RotationX = (RotationX + MathHelper.ToRadians(degrees)) % MathHelper.TwoPi;
        }

        /// <summary>
        /// Moves the camera along the forward axis.
        /// </summary>
        /// <param name="translate">The amount to translate the camera by.</param>
        public void TranslateForward(float translate)
        {
            LookFrom.Z += translate;
        }

        /// <summary>
        /// Moves the camera along right axis.
        /// </summary>
        /// <param name="translate">The amount to translate by. A negative amount goes left.</param>
        public void TranslateRight(float translate)
        {
            LookFrom.X += translate;
        }
    }
}

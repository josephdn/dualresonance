﻿using C3.MonoGame;
using DualResonance.Abilities;
using DualResonance.Deployables.Cards;
using DualResonance.Deployables.Units;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace DualResonance.Graphics
{
    /// <summary>
    /// A singleton class
    /// </summary>
    class AssetManager
    {
        private Dictionary<StatIcon, Rectangle> iconLocations;
        private Dictionary<StatIcon, Color> iconColors;
        private Dictionary<int, Rectangle> mercPortraitRects;
        private AssetManager(Vector2 viewportDims)
        {
            iconLocations = new Dictionary<StatIcon, Rectangle>
            {
                [StatIcon.AP] = APRect,
                [StatIcon.BarrierResonance] = BarrierRect,
                [StatIcon.BurstResonance] = BurstRect,
                [StatIcon.DistortResonance] = DistortRect,
                [StatIcon.HP] = HPRect,
                [StatIcon.NeutralResonance] = NeutralRect,
                [StatIcon.PsycheResonance] = PsycheRect,
                [StatIcon.TriageResonance] = TriageRect,
                [StatIcon.WP] = WPRect
            };

            iconColors = new Dictionary<StatIcon, Color>
            {
                [StatIcon.AP] = Color.Cyan,
                [StatIcon.BarrierResonance] = Color.MediumPurple,
                [StatIcon.BurstResonance] = Color.Yellow,
                [StatIcon.DistortResonance] = Color.Red,
                [StatIcon.HP] = Color.Green,
                [StatIcon.NeutralResonance] = Color.DimGray,
                [StatIcon.PsycheResonance] = Color.Pink,
                [StatIcon.TriageResonance] = Color.LightGreen,
                [StatIcon.WP] = Color.Blue,
            };

            mercPortraitRects = new Dictionary<int, Rectangle>
            {
                [6] = new Rectangle(0, 0, 128, 128),
                [5] = new Rectangle(128, 0, 128, 128),
                [4] = new Rectangle(256, 0, 128, 128),
                [8] = new Rectangle(0, 128, 128, 128),
                [9] = new Rectangle(128, 128, 128, 128),
                [2] = new Rectangle(0, 256, 128, 128),
                [3] = new Rectangle(128, 256, 128, 128),
                [7] = new Rectangle(256, 256, 128, 128),
            };

            ViewportDimensions = viewportDims;
        }

        private Game1 gameRef;
        private SpriteBatch spriteBatchRef;

        /// <summary>
        /// TODO: DON'T DO THIS. Come up with a better solution for the
        /// Fading element.
        /// </summary>
        internal SpriteFont testFont;
        /// <summary>
        /// The size of the screen.
        /// </summary>
        public readonly Vector2 ViewportDimensions;

        private static AssetManager instance;
        /// <summary>
        /// The asset manager singleton.
        /// </summary>
        public static AssetManager Instance
        {
            get
            {
                if (instance == null)
                    throw new MemberAccessException("The Asset Manager needs to be initialized before it can be used.");
                return instance;
            }
        }

        /// <summary>
        /// Prepares the singleton instance for use. Must be called.
        /// </summary>
        /// <param name="game">The game this manager belongs to.</param>
        /// <param name="spriteBatch">The spritebatch through which sprites will be drawn.</param>
        public static void Initialize(Game1 game, SpriteBatch spriteBatch)
        {
            int cMaxHP = CommanderCard.COMMANDER_MAX_HP;
            instance = new AssetManager(
                new Vector2(
                    game.GraphicsDevice.Viewport.Width,
                    game.GraphicsDevice.Viewport.Height
                    ))
            {
                gameRef = game,
                spriteBatchRef = spriteBatch,
                testFont = game.Content.Load<SpriteFont>("temp"),
                commanderUI = game.Content.Load<Texture2D>("commander_ui"),
                menuItemElements = game.Content.Load<Texture2D>("Menus"),
                worldMap = game.Content.Load<Texture2D>("prealpha_copper_worldmap"),
                filledCircle = game.Content.Load<Texture2D>("circle"),
                mercPortraits = game.Content.Load<Texture2D>("micons"),
                fgColorThreshold1 = cMaxHP * .75f,
                fgColorThreshold2 = cMaxHP * .50f,
                fgColorThreshold3 = cMaxHP * .25f,
            };
            instance.blueFrac = (float)Math.Ceiling(cMaxHP - instance.fgColorThreshold1);
        }

        #region Utilities
        /// <summary>
        /// Retrieves yPos of the center of the screen.
        /// </summary>
        /// <returns>The vertical center of the screen.</returns>
        public float GetVerticalCenter() => ViewportDimensions.Y / 2;
        /// <summary>
        /// Retrievest the horizontal center position of the screen.
        /// </summary>
        /// <returns>The horizontal center of the screen.</returns>
        public float GetHorizontalCenter() => ViewportDimensions.X / 2;

        private Vector2 TrueLocation(Vector2 position) => position * gameRef.WindowScaleMultiplier;

        // View matrices
        public Matrix PerspectiveMatrix { get; set; }
        public Matrix ViewMatrix { get; set; } = Matrix.CreateLookAt(
            Vector3.Zero,
            Vector3.UnitZ,
            Vector3.Up
        );

        /// <summary>
        /// Writes the string at the given position.
        /// </summary>
        /// <param name="text">The string to write.</param>
        /// <param name="position">The position on the screen to write the text at.</param>
        /// <param name="color">The color to draw the text in.</param>
        /// <param name="rotation">The angle, in radians, to rotate the text. Defaults to zero.</param>
        public void WriteString(string text, Vector2 position, Color color, float rotation = 0)
        {
            spriteBatchRef.DrawString(
                testFont, 
                text, 
                position * gameRef.WindowScaleMultiplier, 
                color,
                rotation,
                Vector2.Zero,
                gameRef.WindowScaleMultiplier,
                SpriteEffects.None,0);
        }
        /// <summary>
        /// Gets the dimensions of the provided string.
        /// </summary>
        /// <param name="text">The text to measure.</param>
        /// <returns>The size of the text in both directions.</returns>
        public Vector2 MeasureString(string text)
        {
            return testFont.MeasureString(text);
        }

        // thanks stack overflow
        public string WrapText(string text, float maxLineWidth)
        {
            string[] words = text.Split(' ');
            StringBuilder sb = new StringBuilder();
            float lineWidth = 0f;
            float spaceWidth = testFont.MeasureString(" ").X;

            foreach (string word in words)
            {
                Vector2 size = testFont.MeasureString(word);

                if (lineWidth + size.X < maxLineWidth)
                {
                    sb.Append(word + " ");
                    lineWidth += size.X + spaceWidth;
                }
                else
                {
                    sb.Append("\n" + word + " ");
                    lineWidth = size.X + spaceWidth;
                }
            }

            return sb.ToString();
        }

        #region Primitives
        Texture2D filledCircle;
        public void DrawCircle(Vector2 position, Color color, Vector2 scale)
        {
            spriteBatchRef.Draw(filledCircle, position * gameRef.WindowScaleMultiplier, null, color, 0, Vector2.Zero, scale, SpriteEffects.None, 0);
        }
        #endregion

        #endregion Utilities

        #region Stat Icons
        public Texture2D StatIcons { get; set; }

        // These are in pixel dimensions.
        private const int statIconWidth = 1024;
        private const int statIconHeight = 1024;

        private const int TopRow = 0;
        private const int MidRow = 1 * statIconHeight;
        private const int BotRow = 2 * statIconHeight;

        private const int LeftCol = 0;
        private const int MidCol = 1 * statIconWidth;
        private const int RightCol = 2 * statIconWidth;

        public readonly Rectangle NeutralRect =
            new Rectangle(TopRow, LeftCol, statIconWidth, statIconHeight);
        public readonly Rectangle BarrierRect = 
            new Rectangle(MidCol, TopRow, statIconWidth, statIconHeight);
        public readonly Rectangle BurstRect =
            new Rectangle(RightCol, TopRow, statIconWidth, statIconHeight);
        public readonly Rectangle PsycheRect =
            new Rectangle(LeftCol, MidRow, statIconWidth, statIconHeight);
        public readonly Rectangle DistortRect =
            new Rectangle(MidCol, MidRow, statIconWidth, statIconHeight);
        public readonly Rectangle TriageRect =
            new Rectangle(RightCol, MidRow, statIconWidth, statIconHeight);
        public readonly Rectangle HPRect =
            new Rectangle(LeftCol, BotRow, statIconWidth, statIconHeight);
        public readonly Rectangle APRect =
            new Rectangle(MidCol, BotRow, statIconWidth, statIconHeight);
        public readonly Rectangle WPRect =
            new Rectangle(RightCol, BotRow, statIconWidth, statIconHeight);

        public void DrawStatIcon(
            StatIcon icon, 
            Vector2 position, 
            Color? color = null,
            float rotation = 0f,
            float scale = .025f
            )
        {
            Color fColor;
            if (color == null)
                fColor = iconColors[icon];
            else
                fColor = (Color)color;
            spriteBatchRef.Draw(
                StatIcons,
                position * gameRef.WindowScaleMultiplier,
                iconLocations[icon],
                fColor,
                rotation,
                Vector2.Zero,
                new Vector2(scale * gameRef.WindowScaleMultiplier),
                SpriteEffects.None,
                0
            );
        }
        #endregion Stat Icons

        #region Commanders
        public Texture2D commanderUI;
        // Standard dimensions for these UI elements.
        private const int cUIwidth = 256;
        private const int cUIheight = 256;

        public readonly Rectangle CopperIcon1 = new Rectangle(0, 0, cUIwidth, cUIheight);

        public readonly Rectangle CmdrIconBg = new Rectangle(0, cUIheight, cUIwidth, cUIheight);
        public readonly Rectangle CmdrIconFg = new Rectangle(cUIheight, cUIwidth, cUIheight, cUIwidth);

        /// <summary>
        /// Draws the commander icon at the bottom left corner. Will need editing.
        /// </summary>
        /// <param name="commander">The commander being drawn.</param>
        public void DrawCommanderIcon(ActiveCommander commander)
        {
            Rectangle cmdrRect = CopperIcon1;
            // We want to align the portrait with the bottom of the screen, to the right.
            Vector2 finalPosition = new Vector2(
                0,
                gameRef.GraphicsDevice.Viewport.Height - cUIheight * .5f * gameRef.WindowScaleMultiplier
                );
            Vector2 scale = new Vector2(.5f * gameRef.WindowScaleMultiplier);
            // Background
            spriteBatchRef.Draw(
                commanderUI,
                finalPosition,
                CmdrIconBg,
                Color.White,
                0,
                Vector2.Zero,
                scale,
                SpriteEffects.None,
                0);
            // Foreground
            Color fgColor = getFGColor(commander);

            spriteBatchRef.Draw(
                commanderUI,
                finalPosition,
                CmdrIconFg,
                fgColor,
                0,
                Vector2.Zero,
                scale,
                SpriteEffects.None,
                0);

            // Actual commander
            // TODO: Replace with battle-damaged version once available
            spriteBatchRef.Draw(
                commanderUI,
                finalPosition,
                CopperIcon1,
                Color.White,
                0,
                Vector2.Zero,
                scale,
                SpriteEffects.None,
                0
                );
        }

        /// <summary>
        /// The divisor when finding Blue on commander fg. Set at initialization.
        /// </summary>
        private float blueFrac;
        /// <summary>
        /// Defines the limit of the first part of the piecewise commanderHP to
        /// color function.
        /// </summary>
        private float fgColorThreshold1;
        /// <summary>
        /// Defines the limit of the second part of the piecewise commanderHP 
        /// to color function.
        /// </summary>
        private float fgColorThreshold2;
        /// <summary>
        /// Defines the limit of the third part of the piecewise commanderHP to
        /// color function.
        /// </summary>
        private float fgColorThreshold3;

        private Color getFGColor(ActiveCommander commander)
        {
            Color color = new Color();
            color.A = byte.MaxValue;
            if (commander.HealthPoints >= fgColorThreshold1)
            {
                // R=0, G=255, B-.
                color.R = byte.MinValue;
                color.G = byte.MaxValue;
                // B = x/(MaxHP-fgColorThreshold1) with:
                // x = currentHP - fgColorThreshold1
                // blueFrac = (1/(t-h))
                color.B = (byte)((commander.HealthPoints - fgColorThreshold1) / blueFrac * byte.MaxValue);
            }
            else if (commander.HealthPoints >= fgColorThreshold2)
            {
                // R+, G=255, B=0
                color.R = (byte)((1-(commander.HealthPoints - fgColorThreshold2) / fgColorThreshold2) * byte.MaxValue);
                color.G = byte.MaxValue;
                color.B = 0;
            }
            else if (commander.HealthPoints >= fgColorThreshold3)
            {
                // Red=255, G-, B=0.
                color.R = byte.MaxValue;
                color.G = (byte)((commander.HealthPoints - fgColorThreshold3)/ fgColorThreshold3 * byte.MaxValue);
                color.B = byte.MinValue;
            }
            else
            {
                // R-, G=0, B=0.
                color.R = (byte)(commander.HealthPoints / fgColorThreshold3 * byte.MaxValue);
                color.G = byte.MinValue;
                color.B = byte.MinValue;
            }

            return color;
        }
        #endregion 

        #region Menu Items
        Texture2D menuItemElements;
        Rectangle diagonal = new Rectangle(0, 0, 256, 64);
        Rectangle block = new Rectangle(0, 64, 256, 64);

        public void DrawDiagonalItem(Vector2 position, Color color, Vector2 scale)
        {
            spriteBatchRef.Draw(
                menuItemElements,
                position * gameRef.WindowScaleMultiplier,
                diagonal,
                color,
                0,
                Vector2.Zero,
                scale * gameRef.WindowScaleMultiplier,
                SpriteEffects.None,
                0
            );
        }

        public void DrawRectangleItem(Vector2 position, Color color)
        {
            spriteBatchRef.Draw(
                menuItemElements,
                position * gameRef.WindowScaleMultiplier,
                block,
                color,
                0,
                Vector2.Zero,
                new Vector2(gameRef.WindowScaleMultiplier),
                SpriteEffects.None,
                0
            );
        }

        public void RenderMenu<T>(Menu<T> menu, string[] overwrite = null)
        {
            // Center it on the Y position, place it at the x-position.
            float vertScale = .5f;
            float height = diagonal.Height * menu.ItemCount;
            float initialX = ViewportDimensions.X - 110;
            float initialY = (ViewportDimensions.Y - height) / 2;
            initialY = Math.Abs(ViewportDimensions.Y / 2 - diagonal.Height * menu.ItemCount / 2);
            // I should probably just iterate over this three times.
            // Item background
            float yPos = initialY;
            float yOffset = diagonal.Height * vertScale;
            for (int i = 0; i < menu.ItemCount; ++i)
            {
                DrawDiagonalItem(
                    new Vector2(initialX + 10, yPos += yOffset),
                    i == menu.SelectedIndex ? Color.Green : Color.Black,
                    new Vector2(vertScale)
                );
                DrawDiagonalItem(
                    new Vector2(initialX, yPos), 
                    i == menu.SelectedIndex ? Color.Green : Color.Black, 
                    new Vector2(vertScale)
                );
            }
            // Item foreground
            yPos = initialY + .025f * diagonal.Height;
            float xPos = initialX + .025f * diagonal.Width;
            for (int i = 0; i < menu.ItemCount; ++i)
                DrawDiagonalItem(
                    new Vector2(xPos, yPos += yOffset), 
                    Color.White, 
                    new Vector2(vertScale) * .95f
                );

            // Write menu items
            yPos = initialY + testFont.MeasureString("M").Y / 2;
            xPos = initialX + 20;

            for (int i = 0; i < menu.ItemCount; ++i)
            {
                string item = overwrite == null ? menu.GetItem(i).ToString() : overwrite[i];
                WriteString(item, new Vector2(xPos, yPos += yOffset), Color.Black);
            }
        }
        #endregion

        #region Mercenaries

        Texture2D mercPortraits;

        public void DrawMercPortrait(MercenaryCard merc, Vector2 position, float scale = 1, float rotation = 0)
        {
            DrawMercPortrait(merc.Id, position);
        }

        private void DrawMercPortrait(int mercenaryId, Vector2 position, float scale = 1, float rotation = 0)
        {
            try
            {
                Rectangle mSpot = mercPortraitRects[mercenaryId];
                spriteBatchRef.Draw(
                    mercPortraits,
                    position * gameRef.WindowScaleMultiplier,
                    mSpot,
                    Color.White,
                    rotation,
                    Vector2.Zero,
                    scale * gameRef.WindowScaleMultiplier,
                    SpriteEffects.None,
                    0
                    );
            }
            catch
            {
                // We don't have the portrait yet. oops
            }
        }

        public void DrawUnitQuery(ActiveMercenary unit, Vector2 position, bool showGoldCost = false)
        {
            var oPos = position;
            position *= gameRef.WindowScaleMultiplier;
            Rectangle portraitRect = new Rectangle(new Point(33, 0).AddV2ToPoint(position), new Point(47 * (int)gameRef.WindowScaleMultiplier));
            Vector2 portraitPos = new Vector2(33, 0) * gameRef.WindowScaleMultiplier + position;
            Vector2 portraitDim = new Vector2(47 * gameRef.WindowScaleMultiplier);
            Primitives2D.FillRectangle(spriteBatchRef, portraitPos, portraitDim, Color.White, MathHelper.PiOver4);

            Vector2 statusBoxDims = new Vector2(15 * gameRef.WindowScaleMultiplier);
            //action status/team alignment
            Color actionColor = GetActiveUnitColor(unit).ToColor();
            float actionX = 11 * gameRef.WindowScaleMultiplier;
            Primitives2D.FillRectangle(spriteBatchRef, new Vector2(actionX, 0) + position, statusBoxDims, actionColor, MathHelper.PiOver4);
            // Boon (does this mean I'm going to need to design icons for boons now...?)
            float boonX = 54 * gameRef.WindowScaleMultiplier;
            Primitives2D.FillRectangle(spriteBatchRef, new Vector2(boonX, 0) + position, statusBoxDims, Color.LightGreen, MathHelper.PiOver4);
            // ailment (and for ailments as well...?)
            float ailmentY = 44 * gameRef.WindowScaleMultiplier;
            Primitives2D.FillRectangle(spriteBatchRef, new Vector2(actionX, ailmentY) + position, statusBoxDims, Color.Lavender, MathHelper.PiOver4);

            DrawStatIcon(StatIcon.HP, new Vector2(50, 50) + oPos, null, 0, .02f);
            WriteString($"[{unit.HealthPoints}/{unit.MaxHP}]", new Vector2(75, 51) + oPos, Color.Black);

            Vector2 statBackDropDims = new Vector2(18 * gameRef.WindowScaleMultiplier);
            float yPos = (int)(70 * gameRef.WindowScaleMultiplier);

            Primitives2D.FillRectangle(spriteBatchRef, new Vector2(15 * gameRef.WindowScaleMultiplier, yPos) + position, statBackDropDims, Color.White, MathHelper.PiOver4);
            DrawStatIcon(StatIcon.AP, new Vector2(3, 70) + oPos);
            WriteString($"[{unit.AttackPower}/{unit.MaxAP}]", new Vector2(31, 73) + oPos, Color.Black);

            Primitives2D.FillRectangle(spriteBatchRef, new Vector2(84 * gameRef.WindowScaleMultiplier, yPos) + (position), statBackDropDims, Color.White, MathHelper.PiOver4);
            DrawStatIcon(StatIcon.WP, new Vector2(71, 70) + oPos);
            WriteString($"[{unit.WillPower}/{unit.MaxWP}]", new Vector2(99, 73) + oPos, Color.Black);

            WriteString(unit.Name, new Vector2(65, 15) + oPos, Color.Black);
            if (showGoldCost)
            {
                int goldCost = MercenaryCards.GetMercenary(unit.MercenaryId).GoldCost;
                WriteString($"[Costs {goldCost} gold]", new Vector2(70, 31) + oPos, Color.Black);
            }

            if (unit.A != null)
            {
                WriteString(unit.A.Name, new Vector2(0, 104) + oPos, Color.Black);
                DrawResonances(unit.A, new Vector2(175, 104) + oPos);
            }
            // I thought about putting ability descriptions here, but I've decided against it.
            // It will take up more space, and I can change it so that selecting a space an enemy
            // is on will instead pull up a menu where the player can select an enemy's ability
            // and see the ability description that way.
            if (unit.B != null)
            {
                WriteString(unit.B.Name, new Vector2(0, 120) + oPos, Color.Black);
                DrawResonances(unit.B, new Vector2(175, 120) + oPos);
            }
            
            /*
            // We'll need a better way to display ability info.
            string u_flavor = WrapText( unit.FlavorText, 200);
            WriteString(u_flavor, new Vector2(0, yPos += vOffset) + position, Color.Black);*/
        }

        private void DrawResonances(Ability ability, Vector2 position)
        {
            float scale = .0175f;
            float iconWidth = statIconWidth * scale;
            float initialXpos = position.X;
            string cost;
            if (ability.TriageRequirement!= 0)
            {
                cost = ability.TriageRequirement.ToString();
                float costW = testFont.MeasureString(cost).X;
                WriteString(cost, new Vector2(initialXpos -= costW, position.Y), Color.Black);
                DrawStatIcon(StatIcon.TriageResonance, new Vector2(initialXpos -= iconWidth, position.Y), null, 0, scale);
            }
            if (ability.DistortRequirement != 0)
            {
                cost = ability.DistortRequirement.ToString();
                float costW = testFont.MeasureString(cost).X;
                WriteString(cost, new Vector2(initialXpos -= costW, position.Y), Color.Black);
                DrawStatIcon(StatIcon.DistortResonance, new Vector2(initialXpos -= iconWidth, position.Y), null, 0, scale);
            }
            if (ability.PsycheRequirement != 0)
            {
                cost = ability.PsycheRequirement.ToString();
                float costW = testFont.MeasureString(cost).X;
                WriteString(cost, new Vector2(initialXpos -= costW, position.Y), Color.Black);
                DrawStatIcon(StatIcon.PsycheResonance, new Vector2(initialXpos -= iconWidth, position.Y), null, 0, scale);
            }
            if (ability.BurstRequirement != 0)
            {
                cost = ability.BurstRequirement.ToString();
                float costW = testFont.MeasureString(cost).X;
                WriteString(cost, new Vector2(initialXpos -= costW, position.Y), Color.Black);
                DrawStatIcon(StatIcon.BurstResonance, new Vector2(initialXpos -= iconWidth, position.Y), null, 0, scale);
            }
            if (ability.BarrierRequirement != 0)
            {
                cost = ability.BarrierRequirement.ToString();
                float costW = testFont.MeasureString(cost).X;
                WriteString(cost, new Vector2(initialXpos -= costW, position.Y), Color.Black);
                DrawStatIcon(StatIcon.BarrierResonance, new Vector2(initialXpos -= iconWidth, position.Y), null, 0, scale);
            }
        }

        public void DrawVsMercenary(ActiveMercenary lUnit, ActiveMercenary rUnit, Vector2 position)
        {

        }

        public Vector3 GetActiveUnitColor(ActiveBase unit)
        {
            if (unit.Team == Teams.Beta)
                return new Vector3(.75f, 0, 0);

            Vector3 c;
            if (unit is ActiveMercenary merc)
            {
                if (merc.CanMove)
                    c = new Vector3(0, 1, 0);
                else if (merc.CanAct)
                    c = new Vector3(0, .25f, .5f);
                else
                    c = new Vector3(.75f);
            }
            else c = new Vector3(.1f);

            return c;
        }
        #endregion

        Texture2D worldMap;

        public void DrawWorldMap(Vector2 position, Color color)
        {
            spriteBatchRef.Draw(worldMap, position, null, color, 0, Vector2.Zero, .78f * gameRef.WindowScaleMultiplier, SpriteEffects.None, 0);
        }
    }
}

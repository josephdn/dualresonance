﻿using Microsoft.Xna.Framework;

namespace DualResonance.Graphics
{
    class LightSource
    {
        public Vector3 AmbientColor;

        public Vector3 KeyLightColor;
        public Vector3 KeyDirection;

        public Vector3 FillLightColor;
        public Vector3 FillDirection;
    }
}

﻿namespace DualResonance.Graphics
{
    enum StatIcon
    {
        HP,
        AP,
        WP,
        NeutralResonance,
        BarrierResonance,
        BurstResonance,
        PsycheResonance,
        DistortResonance,
        TriageResonance
    }
}

﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DualResonance.Graphics
{
    public class Render3D : IRender
    {
        Model model;
        /// <summary>
        /// The world position of this object.
        /// </summary>
        public Vector3 Position;
        /// <summary>
        /// Affects how the model will be scaled.
        /// </summary>
        public Vector3 Scale = Vector3.One;

        public Matrix PerspectiveMatrix { get; set; }
        public Matrix ViewMatrix { get; set; }

        /// <summary>
        /// The specular color to apply to this model.
        /// </summary>
        public Vector3 SpecularColor { get; set; } = Vector3.Zero;

        /// <summary>
        /// The rotation of the model, about the corresponding axis.
        /// </summary>
        public Vector3 Rotation;

        public Render3D(Model model)
        {
            AssetManager am = AssetManager.Instance;
            if (am.PerspectiveMatrix == null || am.ViewMatrix == null)
                throw new NullReferenceException("Cannot create Render3D before AssetManager initialization.");
            
            ViewMatrix = am.ViewMatrix;
            PerspectiveMatrix = am.PerspectiveMatrix;
            this.model = model;
        }


        /// <summary>
        /// Draws the model to the screen.
        /// </summary>
        /// <param name="game">The game drawing the model.</param>
        /// <param name="camera">The camera used to draw the model.</param>
        /// <param name="sceneLight">The lights used in this scene.</param>
        /// <param name="color">The color to make this model.</param>
        internal void Draw(Game game, Camera3D camera, LightSource sceneLight = null, Vector3? color = null, Texture2D texture = null)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];

            model.CopyBoneTransformsTo(transforms);

            game.GraphicsDevice.BlendState = BlendState.Opaque;
            game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            var cameraTransform = camera.Transform;
            Matrix transform = Matrix.CreateRotationX(Rotation.X) * Matrix.CreateRotationY(Rotation.Y) *
                Matrix.CreateScale(Scale) * Matrix.CreateTranslation(Position);

            foreach (ModelMesh mesh in model.Meshes)
            {                
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = transform * transforms[mesh.ParentBone.Index] * cameraTransform;
                    effect.View = ViewMatrix;
                    effect.Projection = PerspectiveMatrix;
                    effect.LightingEnabled = true;

                    if (texture != null)
                    {
                        effect.TextureEnabled = true;
                        effect.Texture = texture;
                    }

                    if (sceneLight == null)
                    {
                        effect.EnableDefaultLighting();
                        if (color != null)
                            effect.DiffuseColor = (Vector3)color;
                    }
                    else
                    {
                        effect.AmbientLightColor = sceneLight.AmbientColor;
                        // Key Light
                        effect.DirectionalLight0.Direction = Vector3.Transform(sceneLight.KeyDirection, camera.Rotation);
                        effect.DirectionalLight0.SpecularColor = sceneLight.KeyLightColor;// SpecularColor;
                        effect.DirectionalLight0.DiffuseColor = (Vector3)color;
                        // Fill Light
                        effect.DirectionalLight1.Direction = sceneLight.FillDirection;
                        effect.DirectionalLight1.DiffuseColor = (Vector3)color;
                        effect.DirectionalLight1.SpecularColor = sceneLight.FillLightColor;
                        // Back Light
                        // We want this pointing towards the camera at all times.
                        effect.DirectionalLight2.Direction = new Vector3(.25f, 0, -1);
                        effect.DirectionalLight2.DiffuseColor = new Vector3(.5f, .5f, .5f);
                        effect.DirectionalLight2.SpecularColor = SpecularColor;
                    }
                }
                mesh.Draw();
            }
        }
    }
}

﻿using Microsoft.Xna.Framework;

namespace DualResonance.Graphics
{
    // TODO: Create abstract implementer of this interface.
    interface IRender
    {
        Matrix PerspectiveMatrix { get; set; }
        Matrix ViewMatrix { get; set; }
        //void Draw(Game game, Matrix transform);
    }
}

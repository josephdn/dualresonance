﻿using Microsoft.Xna.Framework;

namespace DualResonance
{
    static class Extensions
    {
        /// <summary>
        /// Adds a <see cref="Vector2"/> to a <see cref="Point"/>, resulting in a <see cref="Point"/>.
        /// </summary>
        /// <param name="point">The point being used.</param>
        /// <param name="vector2">The vector being added to the point.</param>
        /// <returns>A new Point.</returns>
        public static Point AddV2ToPoint(this Point point, Vector2 vector2)
        {
            return new Point(point.X + (int)vector2.X, point.Y + (int)vector2.Y);
        }

        /// <summary>
        /// Transforms the <see cref="Vector3"/> into a <see cref="Color"/>.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Color ToColor(this Vector3 v)
        {
            return v.ToColor(255);
        }

        /// <summary>
        /// Transforms the <see cref="Vector3"/> into a <see cref="Color"/>.
        /// </summary>
        /// <param name="v">The vector being transformed.</param>
        /// <param name="alpha">The alpha component to insert into the color.</param>
        /// <returns>A color value corresponding to the vector.</returns>
        public static Color ToColor(this Vector3 v, byte alpha)
        {
            return new Color(v.X * 255, v.Y * 255, v.Z * 255, alpha);
        }
    }
}

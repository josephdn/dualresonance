﻿using DualResonance.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DualResonance.GameStates
{
    class WorldMapState : AbstractGameState
    {


        Vector2[] MissionLocations = new Vector2[]{
            new Vector2(235, 428), //At Midway's Gate
            new Vector2(459, 90), //Almanaught
#if !DEMO
            new Vector2(10, 10) //Testing
#endif
        };

        Menu<string> missions = new Menu<string>(new string[]
        {
            "copper1.drl",
            "copper10.drl",
#if !DEMO
            "testing.drl"
#endif
        });

        public WorldMapState()
        {

        }

        public override void Draw(Game1 game, GameTime gameTime)
        {
            AssetManager am = AssetManager.Instance;
            am.DrawWorldMap(Vector2.Zero, Color.White);
            Color c;
            for (int i = 0; i < MissionLocations.Length; ++i)
            {
                c = missions.IsSelected(i) ? Color.Green : Color.Gray;
                am.DrawCircle(MissionLocations[i], c, new Vector2(.15f));
            }
            //am.WriteString(Microsoft.Xna.Framework.Input.Mouse.GetState().Position.ToString(), Vector2.Zero, Color.Black);
        }

        public override void Update(Game1 game, GameTime gameTime)
        {
            if (InputManager.IsDiscreteDown(Keys.Down) || InputManager.IsDiscreteDown(Keys.Left))
                missions.MoveToNext();
            else if (InputManager.IsDiscreteDown(Keys.Up) || InputManager.IsDiscreteDown(Keys.Right))
                missions.MoveToPrevious();
            else if (InputManager.IsDiscreteDown(Keys.Z) || InputManager.IsDiscreteDown(Keys.Enter))
            {
                game.State = new InLevelState(game, missions.SelectedItem);
            }
        }
    }
}

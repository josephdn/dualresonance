﻿using Microsoft.Xna.Framework;

namespace DualResonance.GameStates
{
    abstract class AbstractGameState : IGameState
    {

        /// <summary>
        /// Causes the game to draw.
        /// </summary>
        /// <param name="gameTime">The elapsed time since last draw.</param>
        public abstract void Draw(Game1 game, GameTime gameTime);
        
        /// <summary>
        /// Updates the game.
        /// </summary>
        /// <param name="game">The game being updated.</param>
        /// <param name="gameTime">The elapsed time since last update.</param>
        public abstract void Update(Game1 game, GameTime gameTime);
    }
}

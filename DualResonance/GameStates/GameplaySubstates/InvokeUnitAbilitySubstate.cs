﻿namespace DualResonance.GameStates.GameplaySubstates
{
    class InvokeUnitAbilitySubstate : LimitedMovementSubstate
    {
        readonly AbilityId id;

        public InvokeUnitAbilitySubstate(AbilityId id, BoardCoordinate originalLocation) : base(originalLocation)
        {
            this.id = id;
        }

        protected override void TakeAction(InLevelState superState, IPlayer player)
        {
            superState.level.InvokeAbility(player, OriginalLocation, OriginalLocation.DirectionTo(superState.cursor), id);
        }
    }
}

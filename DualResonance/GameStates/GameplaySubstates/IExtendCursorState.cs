﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DualResonance.GameStates.GameplaySubstates
{
    interface IExtendCursorState
    {
        BoardCoordinate OriginalLocation { get; set; }
    }
}

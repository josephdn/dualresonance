﻿namespace DualResonance.GameStates.GameplaySubstates
{
    class AttackingUnitSubstate : LimitedMovementSubstate
    {
        public AttackingUnitSubstate(BoardCoordinate originalLocation) : base(originalLocation)
        {
        }

        protected override void TakeAction(InLevelState superState, IPlayer player)
        {
            superState.level.InvokeAttack(player, OriginalLocation, OriginalLocation.DirectionTo(superState.cursor));
        }
    }
}

﻿using DualResonance.Deployables.Units;
using DualResonance.GameStates.GameplaySubstates;
using DualResonance.Graphics;
using DualResonance.Graphics.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DualResonance.GameStates
{
    class InLevelState : AbstractGameState
    {
        Camera3D c = new Camera3D();
        BillboardRender almanaught;
        Render3D tile;
        Texture2D unitTex;
        AssetManager am = AssetManager.Instance;
        LightSource lightSource;
        internal Player player;
        internal AIPlayer ai;
        internal AbstractLevelSubstate subState = new FreeCursorSubstate();

        internal const int QueryLocation = 64;

        TemporaryElement tempMessage = null;

        internal GameSession level = new GameSession();
        public BoardCoordinate cursor = new BoardCoordinate(2, 2);

        bool gameOver = false;
        Teams winningTeam = Teams.Neutral;

        internal readonly string scenarioName;

        public InLevelState(Game1 game, string scenario)
        {
            InitAssets(game);
            scenarioName = scenario;
            var assembly = Assembly.GetExecutingAssembly();
            var levelName = assembly.GetManifestResourceNames()
                .Single(str => str.EndsWith(scenario));
            using (Stream stream = assembly.GetManifestResourceStream(levelName))
            {
                var data = LevelBuilder.FromLevel(stream);
                level = data.Item1;
                lightSource = data.Item2;
            }
            player = (Player)level.GetPlayer(0);
            ai = (AIPlayer)level.GetPlayer(1);
            level.Initialize();
            level.GetPlayerCommander(player).Remove += InLevelState_Player_CommanderRemove;
            // Let conditions handle this.
            var aiCommander = level.GetPlayerCommander(ai);
            if (aiCommander != null)
                aiCommander.Remove += (sender, args) => { };
            level.Victory += (sender, team) => { gameOver = true; winningTeam = team; };
            c.TranslateForward(10);
            c.RotateX(-45);
        }

        private void InLevelState_Player_CommanderRemove(object sender, DeathEventArgs args)
        {
            gameOver = true;
        }

        private void InitAssets(Game1 game)
        {
            // Hm... is this correct? I don't think it is... but it may be fine.
            // Perhaps in the future, we instead just load the models and
            // assets that will be used in the game and/or level.
            almanaught = new BillboardRender(game.Content.Load<Model>("plane"))
            {
                Position = new Vector3(0, 5.9f, 0),
            };
            almanaught.Rotation.X = MathHelper.PiOver2;
            tile = new Render3D(game.Content.Load<Model>("tile"));
            unitTex = game.Content.Load<Texture2D>("rhew0_t");
        }

        #region Drawing
        public override void Draw(Game1 game, GameTime gameTime)
        {
            DrawBoard(game);
            
            int vertical_offset = 16;
            int Y = vertical_offset;

            DrawWinConditions();
            DrawCommanderInfo(vertical_offset);
            DrawQueryResult();
            DrawDeckInfo();
            DrawLogMessages();

            if (tempMessage != null)
                tempMessage.Draw(gameTime);

            subState.Draw(this, gameTime);
            am.WriteString(Mouse.GetState().Position.ToString(), Vector2.One, Color.Black);
            am.WriteString(Mouse.GetState().Position.ToString(), Vector2.Zero, Color.White);
        }

        private void DrawBoard(Game1 game)
        {
            Vector3? color = null;
            List<Tuple<Vector3, Texture2D>> drawData = new List<Tuple<Vector3, Texture2D>>();
            const int LOWER_X_BOUND = GameBoard.PLAY_AREA_DIMS_X / 2 * -1;
            const int UPPER_X_BOUND = GameBoard.PLAY_AREA_DIMS_X / 2 + 1;
            const int LOWER_Y_BOUND = GameBoard.PLAY_AREA_DIMS_Y / 2 * -1;
            const int UPPER_Y_BOUND = GameBoard.PLAY_AREA_DIMS_Y / 2 + 1;
            // Doing it this way allows units to be drawn properly. If I end up rotating the stage, however,
            // I may need to swap these again.
            for (int j = LOWER_Y_BOUND; j < UPPER_Y_BOUND; ++j) // y coords
                for (int i = LOWER_X_BOUND; i < UPPER_X_BOUND; i++) // x coords
                {
                    int offsetI = i + 2;
                    int offsetJ = j + 3;
                    BoardCoordinate offset = new BoardCoordinate(offsetI, offsetJ);
                    if (offset == cursor)
                        color = Vector3.UnitY;
                    else if (subState is IExtendCursorState castState
                        && castState.OriginalLocation == offset)
                        color = new Vector3(1, 1, 0);
                    else if (level.IsInTeamBounds(Teams.Alpha, offset))
                        color = new Vector3(0, .5f, 1);
                    else if (level.IsInTeamBounds(Teams.Beta, offset))
                        color = new Vector3(1, 0, 0);
                    else
                        color = new Vector3(.5f, .5f, .5f);
                    tile.Position = new Vector3(i, 0, j);
                    tile.Draw(game, c, null, color);
                    // Draw the mercenary on a tile.
                    ActiveBase unit;
                    if ((unit = level.Query(offset)) != null)
                        drawData.Add(new Tuple<Vector3, Texture2D>(new Vector3(i, 1.15f, j), unitTex));
                }
            foreach (var data in drawData)
            {
                almanaught.Position = data.Item1;
                almanaught.Draw(game, c, data.Item2, null, new Vector3(.6f, 1, 1));
            }
        }

        private void DrawWinConditions()
        {
            int v_offset = 16;
            int y = -v_offset;
            foreach (var cond in level.AlphaWinConditions)
                am.WriteString(cond.ToString(), new Vector2(0, y += v_offset), Color.Black);
        }

        private void DrawCommanderInfo(int vertical_offset)
        {
            int yPos = 480 - 128 - vertical_offset;
            var commander = level.GetPlayerCommander(player);
            am.WriteString(player.CurrentDeck.Commander.Name, new Vector2(128, 480), Color.Black, -MathHelper.PiOver2);
            am.WriteString(
                $"Resonance: {commander.CurrentResonance}" +
                $"(+{commander.ResonancePerTurn})",
                new Vector2(0, yPos), 
                Color.Black);
            am.WriteString(
                $"Gold: {commander.CurrentGold}/{ActiveCommander.MAX_GOLD} (+{commander.GoldPerTurn}/turn)",
                new Vector2(0, yPos -= vertical_offset), 
                Color.Black);
            am.WriteString(
                commander.GeneratedResonances.ToString(),
                new Vector2(128 + vertical_offset, 480),
                Color.Black,
                -MathHelper.PiOver2);
            am.WriteString(
                $"[HP: {commander.HealthPoints}/{commander.MaxHP}]",
                new Vector2(0, yPos -= vertical_offset),
                Color.Black);
            am.DrawCommanderIcon(commander);

            var aicmdr = level.GetPlayerCommander(ai);
            if (aicmdr == null) return;
            am.WriteString(
                $"Enemy Commander HP: [{aicmdr.HealthPoints}/{aicmdr.MaxHP}]",
                new Vector2(575, 0),
                Color.Black);
        }

        private void DrawQueryResult()
        {
            ActiveMercenary merc;
            if ((merc = (ActiveMercenary)level.Query(cursor)) != null)
            {
                // TODO: Change this
                am.DrawUnitQuery(merc, new Vector2(0, QueryLocation));
            }
        }

        private void DrawDeckInfo()
        {
            /*DeckInfo deck = player.CurrentDeck;
            float xPosDiff = am.MeasureString("Ly").Y;
            float xPos = 800 / 2 - xPosDiff * player.CurrentDeck.MercenaryIds.Count / 2;
            for (int i = 0; i < player.CurrentDeck.MercenaryIds.Count; ++i)
            {
                var current = deck.GetMercenary(i);
                string name = current != null ? current.Name : "---";
                Color fontColor = Color.Black;
                am.WriteString(name, new Vector2(xPos, am.ViewportDimensions.Y), fontColor, -MathHelper.PiOver2);
                xPos += xPosDiff;
            }*/
        }

        private void DrawLogMessages()
        {
            // How many log messages do we want to display?
            // As many as possible. I'll need to implement some kind of scrolling UI element or something.
            var log = Logger.Log.Instance;
            var messages = log.GetItems(8);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < messages.Count; i++)
            {
                builder.AppendLine(messages[i].ToString());
            }
            am.WriteString(builder.ToString(), new Vector2(175, 335), Color.Black);
        }
        #endregion Drawing

        public override void Update(Game1 game, GameTime gameTime)
        {
            if (gameOver && !(subState is EndGameState))
                subState = new EndGameState(game, winningTeam);

            // Manage the camera inputs
            if (InputManager.IsContinuousKeyDown(Keys.S))
                c.TranslateForward(gameTime.ElapsedGameTime.Milliseconds * .05f);
            if (InputManager.IsContinuousKeyDown(Keys.W))
                c.TranslateForward(-gameTime.ElapsedGameTime.Milliseconds * .05f);
            if (InputManager.IsContinuousKeyDown(Keys.A))
                c.RotateY(gameTime.ElapsedGameTime.Milliseconds * .25f);
            if (InputManager.IsContinuousKeyDown(Keys.D))
                c.RotateY(gameTime.ElapsedGameTime.Milliseconds * -.25f);
            if (InputManager.IsContinuousKeyDown(Keys.F))
                c.RotateX(gameTime.ElapsedGameTime.Milliseconds * .25f);
            if (InputManager.IsContinuousKeyDown(Keys.R))
                c.RotateX(gameTime.ElapsedGameTime.Milliseconds * -.25f);
            if (level.ActivePlayer != player && !gameOver)
                foreach (var c in ai.BuildTurn(level))
                    c.Apply(level, out string msg);
            if (tempMessage != null)
                tempMessage.Update(gameTime);
            subState.Update(this, gameTime);
        }

        internal void RegisterTemporaryMessage(string message)
        {
            tempMessage = new FadingTextBox(new Vector2(800 / 2, 0), Vector2.One, 2000, message, am.testFont, TextAlignment.Center, .25f);
            tempMessage.Expire += TempMessage_Expire;
        }

        private void TempMessage_Expire(object sender, ExpireEventArgs e)
        {
            tempMessage = null;
        }
    }
}

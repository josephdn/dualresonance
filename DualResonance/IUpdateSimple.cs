﻿using Microsoft.Xna.Framework;

namespace DualResonance
{
    interface IUpdateSimple
    {
        void Update(Game1 game, GameTime gameTime);
    }
}

﻿using Microsoft.Xna.Framework;

namespace DualResonance
{
    interface IDrawSimple
    {
        void Draw(Game1 game, GameTime gameTime);
    }
}
